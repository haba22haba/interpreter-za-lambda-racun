package interpreter.test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.Before;

import interpreter.data.tree.*;
import interpreter.data.tree.Function.Fun;
import interpreter.logic.machine.*;

public class ExampleTest {

	public Machine machine;
	public Node input;
	public Node output;
	public Node result;

	@Test
	public void testConstants() {
		/* 5 -> 5 */
		input = new Constant(false, 5);
		output = new Constant(false, 5);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* 0 -> 0 */
		input = new Constant(false, 0);
		output = new Constant(false, 0);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* -100 -> --100 */
		input = new Constant(false, -100);
		output = new Constant(false, -100);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testVariables() {
		/* x -> x */
		input = new Variable("x");
		output = new Variable("x");
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testAddition() {
		/* + 5 3 -> 8 */
		input = new ExpApp( 
					new ExpApp(
						new Function(Fun.ADD),
						new Constant(false, 5)
					),
					new Constant(false, 3)
				);
		output = new Constant(false, 8);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* + (-3) 2 -> -1 */
		input = new ExpApp(
					new ExpApp(
						new Function(Fun.ADD),
						new Constant(false, -3)
					),
					new Constant(false, 2)
		);
		output = new Constant(false, -1);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testSubtraction() {
		/* - 5 3 -> 2 */
		input = new ExpApp( 
					new ExpApp(
						new Function(Fun.SUB),
						new Constant(false, 5)
					),
					new Constant(false, 3)
				);
		output = new Constant(false, 2);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* - (-3) (-2) -> -1 */
		input = new ExpApp(
					new ExpApp(
						new Function(Fun.SUB),
						new Constant(false, -3)
					),
					new Constant(false, -2)
		);
		output = new Constant(false, -1);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testMultiplication() {
		/* * 5 3 -> 15 */
		input = new ExpApp( 
					new ExpApp(
						new Function(Fun.MUL),
						new Constant(false, 5)
					),
					new Constant(false, 3)
				);
		output = new Constant(false, 15);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* * (-7) 13 -> -91 */
		input = new ExpApp( 
					new ExpApp(
						new Function(Fun.MUL),
						new Constant(false, -7)
					),
					new Constant(false, 13)
				);
		output = new Constant(false, -91);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testDivision() {
		/* / 18 2 -> 9 */
		input = new ExpApp( 
					new ExpApp(
						new Function(Fun.DIV),
						new Constant(false, 18)
					),
					new Constant(false, 2)
				);
		output = new Constant(false, 9);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testComplicatedExpr() {
		/* + (* ( / 10 2) (% 5 3) )  (- 7 3) -> 14 */
		input = new ExpApp(
					new ExpApp(
						new Function(Function.Fun.ADD),
						new ExpApp(
							new ExpApp(
								new Function(Function.Fun.MUL),
								new ExpApp(
									new ExpApp(
										new Function(Function.Fun.DIV),
										new Constant(false, 10)
									),
									new Constant(false, 2)
								)
							)
							,new ExpApp(
								new ExpApp(
									new Function(Function.Fun.MOD),
									new Constant(false, 5)
								),
								new Constant(false, 3)
							)
						)
					),
					new ExpApp(
						new ExpApp(
							new Function(Function.Fun.SUB),
							new Constant(false, 7)
						),
						new Constant(false, 3)
					)
				);
		output = new Constant(false, 14);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testIf() {
		/* IF 1 1 0 -> 1 */
		input = new ExpApp(
					new ExpApp(
						new ExpApp(
							new Function(Fun.IF),
							new Constant(false, 1)
						),
						new Constant(false, 1)
					),
					new Constant(false, 0)
				);
		output = new Constant(false, 1);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* IF 0 1 0 -> 0 */
		input = new ExpApp(
			new ExpApp(
				new ExpApp(
					new Function(Fun.IF),
					new Constant(false, 0)
				),
				new Constant(false, 1)
			),
			new Constant(false, 0)
		);
		output = new Constant(false, 0);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testLists() {
		/* NIL -> NIL */
		input  = new Constant(true, 0);
		output = new Constant(true, 0);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* CONS 1 NIL -> CONS 1 NIL */
		input = new ConsCell(
					new Constant(false, 1),
					new Constant(true, 0)
				);
		output = input;
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* HEAD (CONS 1 NIL) -> 1 */
		input = new ExpApp(
					new Function(Function.Fun.HEAD),
					new ConsCell(new Constant(false, 1), new Constant(true, 0))
				);
		output = new Constant(false, 1);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* TAIL (CONS 1 NIL) -> NIL */
		input = new ExpApp(
					new Function(Function.Fun.TAIL),
					new ConsCell(new Constant(false, 1),new Constant(true, 0)
			)
		);
		output = new Constant(true, 0);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testLambdaExpressions() {
		/* (\x . x) -> (\x . x) */
		input = new LambdaAbs(new Variable("x"), new Variable("x"));
		output = new LambdaAbs(new Variable("x"), new Variable("x"));
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* (\x . x) 10 -> 10 */
		input = new ExpApp(
							 new LambdaAbs(new Variable("x"), 
							 				new Variable("x")),
							 new Constant(false, 10));
		output = new Constant(false, 10);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* (\x . + x x) 5 -> 10 */
		input = new ExpApp(
					new LambdaAbs(new Variable("x"),
						new ExpApp( 
							new ExpApp( 
								new Function(Fun.ADD),
								new Variable("x")
							),
							new Variable("x")
						)
					), 
					new Constant(false, 5)
				);
		output = new Constant(false, 10);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);

		/* (\x . \y . * x y) 2 3  -> 6 */
		input = new ExpApp(
					new ExpApp(
						new LambdaAbs(new Variable("x"),
							new LambdaAbs(new Variable("y"),
								new ExpApp(
									new ExpApp(
										new Function(Fun.MUL),
										new Variable("x")
									),
									new Variable("y")
								)
							)
						),
						new Constant(false, 2)
					), 
					new Constant(false, 3)
				);
		output = new Constant(false, 6);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testFacotiral() {
		/* Y (\fac . \n . IF (== n 0) 1 (* n fac (- n 1))) 5 -> 120 */
		input = new ExpApp(
					new ExpApp(
						new Function(Function.Fun.Y),
						new LambdaAbs(new Variable("fac"),
							new LambdaAbs(new Variable("n"), 
								new ExpApp(
									new ExpApp(
										new ExpApp(
											new Function(Function.Fun.IF),
											new ExpApp(
												new ExpApp(
													new Function(Function.Fun.EQ),
													new Variable("n")
												),
												new Constant(false, 0)
											)
										),
										new Constant(false, 1)
									),
									new ExpApp(
										new ExpApp(
											new Function(Function.Fun.MUL),
											new Variable("n")
										),
										new ExpApp(
											new Variable("fac"),
											new ExpApp(
												new ExpApp(
													new Function(Function.Fun.SUB),
													new Variable("n")
												),
												new Constant(false, 1)
											)
										)
									)
								)
							)
						)	
					),
					new Constant(false, 20)
				);
		output = new Constant(false, 2432902008176640000);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testSumList() {
		/* Y (\sum . \x . IF (== NIL (HEAD x)) 0 (+ (HEAD x) sum (TAIL x)))) [5,12,7] -> 24 */
		Node fun = new LambdaAbs(new Variable("sum"),
						new LambdaAbs(new Variable("x"),
							new ExpApp(
								new ExpApp(
									new ExpApp(
										new Function(Function.Fun.IF),
										new ExpApp(
											new ExpApp(
												new Function(Function.Fun.EQ),
												new Constant(true, 0)
											),
											new Variable("x")
										)
									),
									new Constant(false, 0)
								),
								new ExpApp(
									new ExpApp(
										new Function(Function.Fun.ADD),
										new ExpApp(
											new Function(Function.Fun.HEAD),
											new Variable("x")
										)
									),
									new ExpApp(
										new Variable("sum"),
										new ExpApp(
											new Function(Function.Fun.TAIL),
											new Variable("x")
										)
									)
								)
							)
						)
					);
		Node arg = new ConsCell(
						new Constant(false, 5),
						new ConsCell(
							new Constant(false, 12),
							new ConsCell(
								new Constant(false, 7),
								new Constant(true, 0)
							)
						)
					);
		input = new ExpApp(
						new ExpApp(
							new Function(Function.Fun.Y),
							fun),
						arg);
		output = new Constant(false, 24);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}

	@Test
	public void testMultipleFun() {
		/*
		 * f = (\x . + x x)
		 * g = (+ 123 (f 5))
		 * 
		 * + (f 3) (- g (f 2)) -> 16
		 */
		Node f = new LambdaAbs(new Variable("x"),
				new ExpApp(
					new ExpApp(
						new Function(Function.Fun.ADD),
						new Variable("x")
					),
					new Variable("x")
				)
			);
		Node g = new ExpApp(
					new ExpApp(
						new Function(Function.Fun.ADD),
						new Constant(false, 123)
					),
					new ExpApp(f, new Constant(false, 5))
				);
		input = new ExpApp(
					new ExpApp(
						new Function(Function.Fun.ADD),
						new ExpApp(
							f,
							new Constant(false, 3)
						)
					),
					new ExpApp(
						new ExpApp(
							new Function(Function.Fun.SUB),
							g
						),
						new ExpApp(f, new Constant(false, 2))
					)
				);
		output = new Constant(false, 135);
		machine = new Machine(input);
		machine.evaluateProgram();
		result = machine.getResult();
		assertEquals(output, result);
	}
}
