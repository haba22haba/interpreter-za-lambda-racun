/**
 * @author Lovro Habjan
 */
package interpreter.logic.input;

import java.util.*;
import interpreter.data.tree.*;

public class Tokenizer {

	public Scanner scanner;

	public Tokenizer() {
		this.scanner = new Scanner(System.in);
	}

	public Node readInput() {
		Node result = null;
		if (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			switch (line.charAt(0)) {
				case '@': {
					result = createExpApp();
					break;
				}
				case 'N': {
					result = createCons(line);
					break;
				}
				case 'F': {
					result = createFun(line);
					break;
				}
				case 'L': {
					result = createLambdaAbs(line); 
					break;
				}
				case 'V': {
					result = createVar(line);
					break;
				}
				case 'C': {
					result = createConsCell();
					break;
				}
				default: {
					break;
				}
			}
		}
		return result;
	}

	private Node createExpApp() {
		Node left = readInput();
		Node right = readInput();
		return new ExpApp(left, right);
	}

	private Node createCons(String line) {
		String arg = line.split(" ")[1];
		if (arg.equals("NIL")) {
			return new Constant(true, 0);
		} else {
			int value = Integer.parseInt(arg);
			return new Constant(false, value);
		}
	}

	private Node createFun(String line) {
		String symbol = line.split(" ")[1];
		if (symbol.equals("+")) {
			return new Function(Function.Fun.ADD);
		} else if (symbol.equals("-")) {
			return new Function(Function.Fun.SUB);
		} else if (symbol.equals("/")) {
			return new Function(Function.Fun.DIV); 
		} else if (symbol.equals("*")) {
			return new Function(Function.Fun.MUL);
		} else if (symbol.equals("%")) {
			return new Function(Function.Fun.MOD);
		} else if (symbol.equals("==")) {
			return new Function(Function.Fun.EQ);
		} else if (symbol.equals("!=")) {
			return new Function(Function.Fun.NEQ);
		} else if (symbol.equals("<")) {
			return new Function(Function.Fun.LTH);
		} else if (symbol.equals(">")) {
			return new Function(Function.Fun.GTH);
		} else if (symbol.equals("<=")) {
			return new Function(Function.Fun.LEQ);
		} else if (symbol.equals(">=")) {
			return new Function(Function.Fun.GEQ);
		} else if (symbol.equals("IF")) {
			return new Function(Function.Fun.IF);
		} else if (symbol.equals("HEAD")) {
			return new Function(Function.Fun.HEAD);
		} else if (symbol.equals("TAIL")) {
			return new Function(Function.Fun.TAIL);
		} else if (symbol.equals("Y")) {
			return new Function(Function.Fun.Y);
		} else {
			return null;
		}
	}

	private Node createLambdaAbs(String line) {
		String var = line.split(" ")[1];
		Node expr = readInput();
		return new LambdaAbs(new Variable(var), expr);
	}

	private Node createVar(String line) {
		String name = line.split(" ")[1];
		return new Variable(name);
	}

	private Node createConsCell() {
		Node head = readInput();
		Node tail = readInput();
		return new ConsCell(head, tail);
	}
}