/**
 * @author Lovro Habjan
 */
package interpreter.logic.machine;

import java.util.Stack;

import interpreter.data.tree.*;
import interpreter.data.tree.Function.Fun;

public class Machine {

	public enum State {
		UNWIND, LAMBDA_RESOLVE, FUNCTION_RESOLVE
	}

	private static Stack<Node> progStack;

	public Stack<ProgState> stateStack;

	public Stack<Node> lambdaCreationStack;

	public ProgState program;

	public Machine(Node node) {
		progStack = new Stack<Node>();
		stateStack = new Stack<ProgState>();
		program = new ProgState(node);
	}

	public void evaluateProgram() {
		while (!isWHNF()) {
	//		System.out.println(program.toString());
			switch (program.state) {
				case UNWIND: {
					if (program.pc instanceof Constant) {
						unwindCheckConstant();
					} else if (program.pc instanceof ConsCell) {
						unwindCheckConsCell();
					} else if (program.pc instanceof ExpApp) {
						unwindGoLeft();
					} else if (program.pc instanceof LambdaAbs) {
						unwindHandleLambdaAbs();
					} else if (program.pc instanceof Variable) {
						System.out.println("ERROR!\tUnresolved variable!");
						System.exit(1);
					} else if (program.pc instanceof Function) {
						unwindHandleFunction();
					} else {
						System.out.println("ERROR!\tUNKNOWN TYPE OF PROGRAM COUNTER");
						System.exit(1);
					}
					break;
				}
				case LAMBDA_RESOLVE: {
					if (program.pc instanceof ExpApp) {
						lambdaHandleExpApp();
					} else if (program.pc instanceof LambdaAbs) {
						lambdaHandleLambda();
					} else if (program.pc instanceof Variable) {
						lambdaHandleVariable();
					} else if (program.pc instanceof ConsCell) {
						lambdaHandleConsCell();	
					} else {
						lambdaCreationStack.push(program.pc);
						program.pc = progStack.pop();
					}
					break;
				}
				case FUNCTION_RESOLVE: {
					if (program.numArgs == 3) {
						functionHandleThreeArgs();
					} else if (program.numArgs == 2) { 
						functionHandleTwoArgs();
					} else if (program.numArgs == 1) {
						functionHandleOneArg();
					} else if (program.numArgs == 0) {
						functionEvaluate();
						functionRewriteRoot();
					}
					break;
				}
				default: break;
			}
		}
	}

	public boolean isWHNF() {
		return (program.state == State.UNWIND) ? program.begin.whnf : false;
	}

	/* 
	 * UNWIND METHODS 
	 */
	public void unwindCheckConstant() {
		program.pc.whnf = true;
		if (program.applicationCounter == 0) {
			if (!stateStack.empty()) {
				program = stateStack.pop();
			}
		} else {
			System.out.println("ERROR!\tConstant applied to something!");
			System.exit(1);
		}
	}

	public void unwindCheckConsCell() {
		program.pc.whnf = true;
		if (program.applicationCounter == 0) {
			if (!stateStack.empty()) {
				program = stateStack.pop();
			}
		} else {
			System.out.println("ERROR!\tConsCell applied to something!");
			System.exit(1);
		}
	}

	public void unwindGoLeft() {
		progStack.push(program.pc);
		program.applicationCounter++;
		program.pc = ((ExpApp) program.pc).left;
	}

	public void unwindHandleLambdaAbs() {
		if (program.applicationCounter < 1) {
			unwindLambdaIsWHNF();
		} else {
			unwindLambdaSwitchState();
		}
	}

	public void unwindLambdaIsWHNF() {
		program.pc.setWHNF(true);
		if (!stateStack.empty()) {
			program = stateStack.pop();
		}
	}

	public void unwindLambdaSwitchState() {
		lambdaSetArgument();
		program.var = ((LambdaAbs) program.pc).var;
		program.root = program.pc;
		lambdaSetExpression();
		lambdaCreationStack = new Stack<Node>();
		program.state = State.LAMBDA_RESOLVE;
	}

	public void lambdaSetArgument() {
			program.pc = progStack.pop();
			program.applicationCounter--;
			program.arg = ((ExpApp) program.pc).right;
			progStack.push(program.pc);
			program.applicationCounter++;
			program.pc = ((ExpApp) program.pc).left;
	}

	public void lambdaSetExpression() {
		progStack.push(program.pc);
		program.pc = ((LambdaAbs) program.pc).expression;
	}

	public void unwindHandleFunction() {
		Function fun = (Function) program.pc;
		if (program.applicationCounter < fun.numArgs) {
			unwindFunctionIsWHNF();
		} else {
			unwindFunctionSwitchState();
		}
	}

	/* TODO - set WHNF za ExpApp ki so vezani na to funkcijo */
	public void unwindFunctionIsWHNF() {
		program.pc.setWHNF(true);
		if (!stateStack.empty()) {
			program = stateStack.pop();
		}
	}

	public void unwindFunctionSwitchState() {
		program.function = (Function) program.pc;
		program.numArgs = ((Function) program.pc).numArgs;
		program.state = State.FUNCTION_RESOLVE;
	}

	/*
	 * LAMBDA_RESOLVE METHODS
	 */
	public void lambdaHandleExpApp() {
		ExpApp e = (ExpApp) program.pc;
		if (e.numChecked == 0) {
			lambdaExpAppCheckOnce();
		} else if (e.numChecked == 1) {
			lambdaExpAppCheckTwice();
		} else {
			lambdaExpAppCheckThrice();
		}
	}

	public void lambdaExpAppCheckOnce() {
		((ExpApp) program.pc).numChecked++;
		progStack.push(program.pc);
		program.pc = ((ExpApp) program.pc).left;
	}

	public void lambdaExpAppCheckTwice() {
		((ExpApp) program.pc).numChecked++;
		progStack.push(program.pc);
		program.pc = ((ExpApp) program.pc).right;
	}

	public void lambdaExpAppCheckThrice() {
		lambdaCreateNewExpApp();
		((ExpApp) program.pc).numChecked = 0;
		program.pc = progStack.pop();
	}

	public void lambdaCreateNewExpApp() {
		program.sndOperand = lambdaCreationStack.pop();
		program.fstOperand = lambdaCreationStack.pop();
		program.result = new ExpApp(program.fstOperand, program.sndOperand);
		lambdaCreationStack.push(program.result);
	}

	public void lambdaHandleLambda() {
		if (program.pc.equals(program.root)) {
			lambdaRewriteRoot();
		} else {
			if (((LambdaAbs) program.pc).numChecked == 0) {
				if (((LambdaAbs) program.pc).var.equals(program.var)) {
					lambdaCreationStack.push(program.pc);
					program.pc = progStack.pop();
				} else {
					((LambdaAbs) program.pc).numChecked++;
					progStack.push(program.pc);
					program.pc = ((LambdaAbs) program.pc).expression;
				}
			} else {
				((LambdaAbs) program.pc).numChecked = 0;
				lambdaCreateNewLambda();
				program.pc = progStack.pop();
			}
		}
	}

	public void lambdaRewriteRoot() {
		program.result = lambdaCreationStack.pop();
		program.pc = progStack.pop();
		program.applicationCounter--;
		if (program.applicationCounter == 0) {
			program.pc = program.result;
			program.begin = program.pc;

		} else {
			lambdaAttachRootToFather();
		}
		program.state = State.UNWIND;
	}

	public void lambdaAttachRootToFather() {
		program.pc = progStack.pop();
		program.applicationCounter--;
		if (program.pc instanceof ExpApp) {
			ExpApp rootOfIndex = (ExpApp) program.pc;
			if (rootOfIndex.numChecked == 0) {
				rootOfIndex.left = program.result;
			} else {
				rootOfIndex.right = program.result;
			}
		} else {
			System.exit(1);
			System.out.println("ERROR\tLambda abstraction not applicated to ExpApp!");
		}
	}

	public void lambdaCreateNewLambda() {
		program.fstOperand = lambdaCreationStack.pop();
		program.result = new LambdaAbs(((LambdaAbs) program.pc).var, program.fstOperand);
		lambdaCreationStack.push(program.result);
	}

	public void lambdaHandleVariable() {
		Variable e = (Variable) program.pc;
		if (e.equals(program.var)) {
			lambdaCreationStack.push(program.arg);
		} else {
			lambdaCreationStack.push(e);
		}
		program.pc = progStack.pop();
	}

	public void lambdaHandleConsCell() {
		ConsCell e = (ConsCell) program.pc;
		if (e.numChecked == 0) {
			e.numChecked++;
			progStack.push(program.pc);
			program.pc = e.head;
		} else if (e.numChecked == 1) {
			progStack.push(program.pc);
			e.numChecked++;
			program.pc = e.tail;
		} else {
			e.numChecked = 0;
			program.pc = progStack.pop();
		}
	}

	/*
	 * FUNCTION_RESOLVE
     */

	/* Functions: IF */
	public void functionHandleThreeArgs() {
		functionMoveRight();
		functionHandleIfCondition();
	}
	
	public void functionHandleIfCondition() {
		if (program.pc instanceof Constant) {
			program.fstOperand = program.pc;
			program.pc = progStack.pop();
			((ExpApp) program.pc).numChecked = 0;
			program.numArgs--;
		} else if (program.pc instanceof ExpApp) {
			if (program.pc.whnf) {
				program.begin.whnf = true;
				program.state = State.UNWIND;
			} else {
				saveStateMachine();
				setStateMachine(program.pc);
			}
		}
	}

	/* Functions: IF, numerični, primerjalni, CONS */
	public void functionHandleTwoArgs() {
		functionMoveRight();
		switch (program.function.fun) {
			case IF: {
				functionHandleIfTrue();
				break;
			}
			case ADD: case SUB: case DIV: case MUL: case MOD:
			case EQ: case NEQ: case LTH: case GTH: case LEQ: case GEQ: {
				functionHandleNumericFstArg();
				break;
			}
			default: {
				System.out.println("ERROR\tFAILED FUNCTION_RESOLVE");
				System.exit(1);
				break;
			}
		}
	}

	public void functionHandleIfTrue() {
		program.sndOperand = program.pc;
		program.pc = progStack.pop();
		((ExpApp) program.pc).numChecked = 0;
		program.numArgs--;
	}

	public void functionHandleNumericFstArg() {
		if (program.pc instanceof Constant) {
			program.fstOperand = program.pc;
			program.pc = progStack.pop();
			((ExpApp) program.pc).numChecked = 0;
			program.numArgs--;
		} else if (program.pc instanceof ExpApp) {
			if (program.pc.whnf) {
				program.begin.whnf = true;
				program.state = State.UNWIND;
			} else {
				saveStateMachine();
				setStateMachine(program.pc);
			}
		}
	}

	/* Functions: IF, numerični, primerjalni, Y, HEAD, TAIL */
	public void functionHandleOneArg() {
		functionMoveRight();
		switch (program.function.fun) {
			case IF: {
				functionHandleIfFalse();
				break;
			}
			case ADD: case SUB: case DIV: case MUL: case MOD:
			case EQ: case NEQ: case LTH: case GTH: case LEQ: case GEQ: {
				functionHandleNumericSndArg();
				break;
			}
			case Y: {
				functionHandleYArg();
				break;
			}
			case HEAD: case TAIL: {
				functionHandleListArg();
				break;
			}
			default: {
				System.out.println("ERROR\tFAILED FUNCTION_RESOLVE");
				System.exit(1);
			}
		}
	}

	public void functionHandleIfFalse() {
		program.trdOperand = program.pc;
		program.pc = progStack.pop();
		((ExpApp) program.pc).numChecked = 0;
		program.numArgs--;
	}

	public void functionHandleNumericSndArg() {
		if (program.pc instanceof Constant ||
			program.pc instanceof ConsCell) {
			program.sndOperand = program.pc;
			program.pc = progStack.pop();
			((ExpApp) program.pc).numChecked = 0;
			program.numArgs--;
		} else if (program.pc instanceof ExpApp) {
			if (program.pc.whnf) {
				program.begin.whnf = true;
				program.state = State.UNWIND;
			} else {
				saveStateMachine();
				setStateMachine(program.pc);
			}
		}
	}

	public void functionHandleYArg() {
		if (program.pc instanceof LambdaAbs) {
			program.fstOperand = program.pc;
			program.pc = progStack.pop();
			((ExpApp) program.pc).numChecked = 0;
			program.numArgs--;
		} else {
			System.out.println("ERROR\tY DOES NOT HAVE FUNCTION AS AN ARGUMENT");
			System.exit(1);
		}
	}

	public void functionHandleListArg() {
		if (program.pc instanceof ConsCell) {
			program.fstOperand = ((ConsCell) program.pc).head;
			program.sndOperand = ((ConsCell) program.pc).tail;
			program.numArgs--;
			program.pc = progStack.pop();
		} else {
			saveStateMachine();
			setStateMachine(program.pc);
		}
	}

	public void functionEvaluate() {
		switch (program.function.fun) {
			case IF: {
				program.result = (((Constant) program.fstOperand).value != 0) ? program.sndOperand : program.trdOperand;
				break;
			}
			case ADD: {
				program.result = new Constant(false, ((Constant) program.fstOperand).value + ((Constant) program.sndOperand).value);
				break;
			}
			case SUB: {
				program.result = new Constant(false, ((Constant) program.fstOperand).value - ((Constant) program.sndOperand).value);
				break;
			}
			case DIV: {
				program.result = new Constant(false, ((Constant) program.fstOperand).value / ((Constant) program.sndOperand).value);
				break;
			}
			case MUL: {
				program.result = new Constant(false, ((Constant) program.fstOperand).value * ((Constant) program.sndOperand).value);
				break;
			}
			case MOD: {
				program.result = new Constant(false, ((Constant) program.fstOperand).value % ((Constant) program.sndOperand).value);
				break;
			}
			case EQ: {
				program.result = (program.fstOperand.equals(program.sndOperand)) ? new Constant(false, 1) : new Constant(false, 0);
				break;
			}
			case NEQ: {
				program.result = (!program.fstOperand.equals(program.sndOperand)) ? new Constant(false, 1) : new Constant(false, 0);
				break;
			}
			case LTH: {
				program.result = (((Constant) program.fstOperand).value < ((Constant) program.sndOperand).value) ? new Constant(false, 1) : new Constant(false, 0);
				break;
			}
			case GTH: {
				program.result = (((Constant) program.fstOperand).value > ((Constant) program.sndOperand).value) ? new Constant(false, 1) : new Constant(false, 0);
				break;
			}
			case LEQ: {
				program.result =(((Constant) program.fstOperand).value <= ((Constant) program.sndOperand).value) ? new Constant(false, 1) : new Constant(false, 0);
				break;
			}
			case GEQ: {
				program.result = (((Constant) program.fstOperand).value >= ((Constant) program.sndOperand).value) ? new Constant(false, 1) : new Constant(false, 0);
				break;
			}
			case HEAD: {
				program.result = program.fstOperand;
				break;
			}
			case TAIL: {
				program.result = program.sndOperand;
				break;
			}
			case Y: {
				program.sndOperand = new ExpApp(new Function(Function.Fun.Y), program.fstOperand);
				program.result = new ExpApp(program.fstOperand, program.sndOperand);
				break;
			}
			default:
				break;
		}
	}

	public void functionMoveRight() {
		program.pc = progStack.pop();
		((ExpApp) program.pc).numChecked++;
		progStack.push(program.pc);
		program.pc = ((ExpApp) program.pc).right;
	}


	public void functionRewriteRoot() {
		if (program.function.fun == Function.Fun.Y) {
			program.pc = progStack.pop();
			program.applicationCounter -= 2;
			if (program.pc.equals(program.begin)) {
				((ExpApp) program.pc).left = program.result;
				program.begin = program.pc;
			} else {
				((ExpApp) program.pc).left = program.result;
			}
			program.state = State.UNWIND;
		} else {
			if (stateStack.empty()) {
				program.state = State.UNWIND;
				program.begin = program.result;
				program.pc = program.result;
			} else {
				progStack.push(program.result);
				program = stateStack.pop();
				program.pc = progStack.pop();
				program.root = program.pc;
				functionAttachRootToFather();
			}
		}
	}

	public void functionAttachRootToFather() {
		program.pc = progStack.pop();
		program.applicationCounter--;
		if (program.pc instanceof ExpApp) {
			ExpApp rootOfIndex = (ExpApp) program.pc;
			if (rootOfIndex.numChecked == 0) {
				rootOfIndex.left = program.root;
				progStack.push(program.pc);
				program.pc = rootOfIndex.left;
			} else {
				rootOfIndex.right = program.root;
				progStack.push(program.pc);
				program.pc = rootOfIndex.right;
			}
		} else if (program.pc instanceof LambdaAbs) {
			LambdaAbs rootOfIndex = (LambdaAbs) program.pc;
			rootOfIndex.expression = program.root;
		} else {
			System.out.println("ERROR\tFunction result not attached to ExpApp or LambdaAbs!");
			System.exit(1);
		}
	}

	/*
	 * GENERAL METHODS
	 */
	public void saveStateMachine() {
		stateStack.push(program);
	}

	public void setStateMachine(Node node) {
		program = new ProgState(node);
	}

	public void resetStateMachine() {
		program = stateStack.pop();
	}

	public Node getResult() {
		return program.begin;
	}
}

class ProgState {

	public int applicationCounter;

	public Machine.State state;

	public Node pc;

	public Node arg;

	public Variable var;

	public Node root;

	public Node begin;

	public int numArgs;

	public Node fstOperand;
	public Node sndOperand;
	public Node trdOperand;
	public Node result;
	public Function function;

	public ProgState(Node node) {
		applicationCounter = 0;
		state = Machine.State.UNWIND;
		pc = node;
		arg = null;
		var = null;
		root = null;
		begin = node;
		numArgs = 0;
		fstOperand = null;
		sndOperand = null;
		trdOperand = null;
		result = null;
		function = null;
	}

	@Override
	public String toString() {
		return "PC:\t\t" + pc + "\n" +
			   "STATE:\t\t" + state + "\n" +
			   "BEGIN:\t\t" + begin + "\n" +
			   "ROOT:\t\t" + root + "\n" +
			   "APPCOUNT:\t" + applicationCounter + "\n" +
			   "ARG:\t\t" + arg + "\n" +
			   "FUNCTION:\t" + function + "\n" + 
			   "NUMARGS:\t" + numArgs + "\n" + 
			   "FSTOP:\t\t" + fstOperand + "\n" +
			   "SNDOP:\t\t" + sndOperand + "\n" +
			   "TRDOP:\t\t" + trdOperand + "\n" +
			   "RESULT:\t\t" + result + "\n";
	}
}