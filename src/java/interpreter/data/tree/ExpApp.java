/**
 * Expression application
 * 
 * @author Lovro Habjan
 */
package interpreter.data.tree;

public class ExpApp extends Node {

	public Node left;
	public Node right;
	public int numChecked;

	public ExpApp(Node left, Node right) {
		super();
		this.left = left;
		this.right = right;
		setWHNF(false);
		numChecked = 0;
	}

	@Override
	public String toString() {
		return "@  ( " + left.toString() + " ) ( " + right.toString() + " )";
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof ExpApp) {
			ExpApp otherExp = (ExpApp) other;
			return left.equals(otherExp.left) && right.equals(otherExp.right);
		} else {
			return false;
		}
	}
}