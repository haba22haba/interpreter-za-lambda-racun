/**
 * A built-in function
 *
 * @author Lovro Habjan
 */
package interpreter.data.tree;

public class Function extends Node {

	public enum Fun {
		ADD, SUB, DIV, MUL, MOD, EQ, NEQ, LTH, GTH, LEQ, GEQ, IF, HEAD, TAIL, Y
	}

	public Fun fun;
	public int numArgs;

	public Function(Fun fun) {
		super();
		this.fun = fun;
		switch(this.fun) {
			case ADD: case SUB: case DIV: case MUL: case MOD:
			case EQ: case NEQ: case LEQ: case GEQ: case LTH: case GTH: {
				setWHNF(false);
				this.numArgs = 2;
				break;
			}
			case IF: {
				setWHNF(false);
				this.numArgs = 3;
				break;
			}
			case HEAD: case TAIL: {
				setWHNF(false);
				this.numArgs = 1;
				break;
			}
			case Y: {
				setWHNF(false);
				this.numArgs = 1;
				break;
			}
		}
	}

	
	@Override
	public String toString() {
		switch (fun) {
			case ADD: return "+";
			case SUB: return "-";
			case DIV: return "/";
			case MUL: return "*";
			case MOD: return "%";
			case EQ: return "==";
			case NEQ: return "!=";
			case GEQ: return ">=";
			case LEQ: return "<=";
			case LTH: return "<";
			case GTH: return ">";
			case IF: return "IF";
			case HEAD: return "HEAD";
			case TAIL: return "TAIL";
			case Y: return "Y";
			default: return "";
		}
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Function) {
			return fun.equals(((Function) other).fun);
		} else {
			return false;
		}
	}
}