/**
 * @author Lovro Habjan
 */

package interpreter.data.tree;

public class ConsCell extends Node {

	public Node head;
	public Node tail;
	public int numChecked;

	public ConsCell(Node head, Node tail) {
		super();
		this.head = head;
		this.tail = tail;
		setWHNF(true);
		this.numChecked = 0;
	}

	@Override
	public String toString() {
		return "CONS " + head.toString() + " (" + tail.toString() + ")";
	}

	@Override
	public boolean equals(Object object) {
		return (object instanceof ConsCell) ? true : false;
	}
}