package interpreter.data.tree;

public class Variable extends Node {

	public String name;

	public Variable(String name) {
		super();
		this.name = name;
		setWHNF(true);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Variable) {
			return name.equals(((Variable) other).name);
		} else {
			return false;
		}
	}
}