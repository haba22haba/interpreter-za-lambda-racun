package interpreter.data.tree;

public class LambdaAbs extends Node {

	public Variable var;

	public Node expression;

	public int numChecked;

	public LambdaAbs(Variable var, Node expr) {
		super();
		this.var = var;
		this.expression = expr;
		setWHNF(false);
		numChecked = 0;
	}

	@Override
	public String toString() {
		return "\\ " + var.toString() + " . ( " + expression.toString() + " ) ";
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof LambdaAbs) {
			LambdaAbs otherLam = (LambdaAbs) other;
			return var.equals(otherLam.var) && expression.equals(otherLam.expression);
		} else {
			return false;
		}
	}
}