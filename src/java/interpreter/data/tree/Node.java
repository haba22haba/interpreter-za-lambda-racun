/**
 * @author Lovro Habjan
 */
package interpreter.data.tree;

public abstract class Node {
	
	public boolean whnf;

	public Node() {
		this.whnf = false;
	}

	@Override
	public String toString() {
		return "Abstract";
	}

	public boolean isWHNF() {
		return this.whnf;
	}

	public void setWHNF(boolean val) {
		this.whnf = val;
	}
}