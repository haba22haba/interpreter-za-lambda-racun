/**
 * @author Lovro Habjan
 */
package interpreter.data.tree;

public class Constant extends Node {

	public int value;
	public boolean nil;

	public Constant(boolean nil, int val) {
		super();
		this.value = val;
		this.nil = nil;
		setWHNF(true);
	}

	@Override
	public String toString() {
		return (nil) ? "NIL" : Integer.toString(value);
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Constant) {
			Constant cons = (Constant) other;
			if (cons.nil && this.nil) {
				return true;
			} else if (!cons.nil && !this.nil) {
				return value == cons.value;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}