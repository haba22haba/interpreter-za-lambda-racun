/**
 * @author Lovro Habjan
 */
package interpreter;

import interpreter.logic.input.*;
import interpreter.logic.machine.Machine;
import interpreter.data.tree.*;
import java.util.*;

public class Main {

	public static void main(String[] args) {
		Tokenizer tokenizer = new Tokenizer();
		Node input = tokenizer.readInput();
		tokenizer.scanner.close();
		System.out.println(input.toString());
		Machine machine = new Machine(input);
		machine.evaluateProgram();
		Node node = machine.getResult();
		System.out.println(node.toString());
	}
}