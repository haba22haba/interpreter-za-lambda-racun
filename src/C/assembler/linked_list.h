#ifndef _LINKEDLIST_H_
#define _LINKEDLIST_H_

#include <stdio.h>
#include <stdlib.h>

#include "node.h"

typedef struct _input_element_t {
	char *line;
	int line_num;
	struct _input_element_t *next;
} input_element_t;

typedef struct _input_list_t {
	input_element_t *first;
	input_element_t *last;
} input_list_t;

typedef struct _subtree_element_t {
	uint32_t node;
	int line_num;
	struct _subtree_element_t *next;
} subtree_element_t;

typedef struct _subtree_list_t {
	subtree_element_t *first;
	subtree_element_t *last;
} subtree_list_t;

input_list_t *input_list;

subtree_list_t *subtree_list;

void list_create_input_list();
void list_input_add(input_element_t *el);
input_element_t *list_input_create_element(char *s, int n);
input_element_t *list_input_find_element(int n);
void list_input_print();
void list_input_clear();

void list_create_subtree_list();
void list_subtree_add(subtree_element_t *el);
subtree_element_t *list_subtree_create_element();
subtree_element_t *list_subtree_find_element(int n);
void list_subtree_clear();

#endif /* _LINKEDLIST_H_ */