#include <string.h>
#include <stdint.h>
#include <string.h>

#include "linked_list.h"

void input_read();

uint32_t create_tree();
uint32_t parse_constant(char *line);
uint32_t parse_variable(char *line);
uint32_t parse_function(char *line);
uint32_t parse_lambda_abs(char *line);
uint32_t parse_cons_cell();
uint32_t parse_exp_app();

int64_t string_to_int(char *string);

void create_output_file();

input_element_t *current_line = NULL;

int main(int argc, char *argv[]) {
	list_create_input_list();
	list_create_subtree_list();
	input_read();

	current_line = input_list->first;
	uint32_t start = create_tree();

	create_output_file();
	list_input_clear();
	list_subtree_clear();
}

void input_read() {
	int line_number = 1;
	char *line;
	size_t length = 0;
	ssize_t read;
	while ((read = getline(&line, &length, stdin)) != -1) {
		char *msg = (char *) malloc((read + 1) * sizeof(char));
		if (msg) {
			strncpy(msg, line, read);
			msg[read-1]	= '\0';
			input_element_t *el = list_input_create_element(msg, line_number);
			list_input_add(el);
		} else {
			fprintf(stderr, "input_read():\t msg malloc failed!\n");
			exit(1);
		}
		line_number++;
	}
	free(line);
}

uint32_t create_tree() {
	if (current_line == NULL) {
		fprintf(stderr, "create_tree()\tcurrent_line is NULL!\n");
		exit(1);
	}
	char node_type = current_line->line[0];

	switch (node_type) {
		case 'N': {
			subtree_element_t *el = list_subtree_create_element();
			el->line_num = current_line->line_num;
			uint32_t constant = parse_constant(current_line->line);
			el->node = constant;
			list_subtree_add(el);
			current_line = current_line->next;

			return el->node;
		}
		case 'V': {
			subtree_element_t *el = list_subtree_create_element();
			el->line_num = current_line->line_num;
			uint32_t var = parse_variable(current_line->line);
			el->node = var;
			list_subtree_add(el);
			current_line = current_line->next;

			return el->node;
		}
		case 'F': {
			subtree_element_t *el = list_subtree_create_element();
			el->line_num = current_line->line_num;
			uint32_t fun = parse_function(current_line->line);
			el->node = fun;
			list_subtree_add(el);
			current_line = current_line->next;

			return el->node;
		}
		case 'L': {
			subtree_element_t *el = list_subtree_create_element();
			el->line_num = current_line->line_num;
			list_subtree_add(el);
			uint32_t lambda = parse_lambda_abs(current_line->line);
			el->node = lambda;

			return el->node;
		}
		case 'C': {
			subtree_element_t *el = list_subtree_create_element();
			el->line_num = current_line->line_num;
			list_subtree_add(el);
			current_line = current_line->next;
			el->node = node_create_cons_cell(0, 0, 1);
			node_set_first_pointer(el->node, el->node);
			node_set_second_pointer(el->node, el->node);
			uint32_t head = create_tree();
			node_set_first_pointer(el->node, head);
			uint32_t tail = create_tree();
			node_set_second_pointer(el->node, tail);

			return el->node;
		}
		case '@': {
			subtree_element_t *el = list_subtree_create_element();
			list_subtree_add(el);
			el->line_num = current_line->line_num;
			current_line = current_line->next;
			el->node = node_create_exp_app(0, 0, 1);
			node_set_first_pointer(el->node, el->node);
			node_set_second_pointer(el->node, el->node);
			uint32_t first = create_tree();
			node_set_first_pointer(el->node, first);
			uint32_t second = create_tree();
			node_set_second_pointer(el->node, second);

			return el->node;
		}
		case 'T': {
			int number;
			sscanf(current_line->line, "%*c %d", &number);
			subtree_element_t *el = list_subtree_find_element(number);
			current_line = current_line->next;
			
			return el->node;
		}
		default: {
			printf("unknown line[0]");
			break;
		}
	}
}

uint32_t parse_constant(char *line) {
	char string[50];
	sscanf(line, "%*c %s", string);
	if (strlen(string) == 3 && strcmp(string, "NIL") == 0) {
		return node_create_constant(0, 1, 1);
	} else {
		return node_create_constant(string_to_int(string), 0, 1);
	}
}

int64_t string_to_int(char *string) {
	int64_t r = 0;
	char *s = string;
	
	int negative = 0;
	if (s[0] == '-') {
		negative = 1;
		s++;
	}

	while (*s != '\0') {
		r = 10*r + *s - 48;
		s++;
	}
	
	return (negative == 0) ? r : -r;
}

uint32_t parse_variable(char *line) {
	int64_t var_tag;
	sscanf(line, "%*c %ld", &var_tag);
	
	return node_create_variable(var_tag, 1);
}
uint32_t parse_function(char *line) {
	char operator[5];
	sscanf(line, "%*c %s", operator);

	if (strcmp(operator, "+") == 0) {
		return node_create_function(ADD, 1);
	} else if (strcmp(operator, "-") == 0) {
		return node_create_function(SUB, 1);
	} else if (strcmp(operator, "*") == 0) {
		return node_create_function(MUL, 1);
	} else if (strcmp(operator, "/") == 0) {
		return node_create_function(DIV, 1);
	} else if (strcmp(operator, "%") == 0) {
		return node_create_function(MOD, 1);
	} else if (strcmp(operator, "Y") == 0) {
		return node_create_function(Y, 1);
	} else if (strcmp(operator, "<") == 0) {
		return node_create_function(LTH, 1);
	} else if (strcmp(operator, ">") == 0) {
		return node_create_function(GTH, 1);
	} else if (strcmp(operator, "==") == 0) {
		return node_create_function(EQ, 1);
	} else if (strcmp(operator, "!=") == 0) {
		return node_create_function(NEQ, 1);
	} else if (strcmp(operator, "<=") == 0) {
		return node_create_function(LEQ, 1);
	} else if (strcmp(operator, ">=") == 0) {
		return node_create_function(GEQ, 1);
	} else if (strcmp(operator, "IF") == 0) {
		return node_create_function(IF, 1);
	} else if (strcmp(operator, "HEAD") == 0) {
		return node_create_function(HEAD, 1);
	} else if (strcmp(operator, "TAIL") == 0) {
		return node_create_function(TAIL, 1);
	} else if (strcmp(operator, "CONS") == 0) {
		return node_create_function(CONS, 1);
	} else {
		fprintf(stderr, "Unkown function!\n");
		exit(1);
		return 0;
	}
}

uint32_t parse_lambda_abs(char *line) {
	int64_t var_tag;
	sscanf(line, "%*c %ld", &var_tag);
	uint32_t r = node_create_lambda_abs(0, 0, 1);
	uint32_t var = node_create_variable(var_tag, 1);
	current_line = current_line->next;
	uint32_t expr = create_tree();
	node_set_first_pointer(r, var);
	node_set_second_pointer(r, expr);
	return r;
}

void create_output_file() {
	FILE *output = fopen("memory.bin", "wb");
	fwrite(memory, 1, sizeof(memory), output);
	fclose(output);
}
