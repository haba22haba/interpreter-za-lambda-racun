#include "node.h"

uint32_t node_free_cell = NODE_SIZE;

uint32_t allocate() {
	uint32_t r = node_free_cell;
	node_free_cell = node_free_cell + NODE_SIZE;
	return r;
}

uint32_t node_create_constant(uint64_t value, int is_nil, int new) {
	uint32_t node = allocate();
	node_reset_cell(node);
	node_set_tag(node, TAG_CONSTANT);
	if (is_nil) {
		node_set_is_nil(node);
	} else {
		node_reset_is_nil(node);
	}
	node_reset_mark(node);
	node_set_whnf(node);
	if (new) {
		node_set_new(node);
	} else {
		node_reset_new(node);
	}

	node_set_body(node, value);

	return node;
}

uint32_t node_create_cons_cell(uint32_t head, uint32_t tail, int new) {
	uint32_t node = allocate();
	node_reset_cell(node);
	node_set_tag(node, TAG_CONSCELL);
	node_reset_mark(node);
	node_set_whnf(node);
	node_set_num_checked(node, 0);
	if (new) {
		node_set_new(node); 
	} else {
		node_reset_new(node);
	}

	node_set_first_pointer(node, head);
	node_set_second_pointer(node, tail);

	return node;
}

uint32_t node_create_variable(int64_t var, int new) {
	uint32_t node = allocate();
	node_reset_cell(node);
	node_set_tag(node, TAG_VARIABLE);
	node_reset_mark(node);
	node_set_whnf(node);
	if (new) {
		node_set_new(node); 
	} else {
		node_reset_new(node);
	}

	node_set_body(node, var);

	return node;
}

uint32_t node_create_lambda_abs(uint32_t var, uint32_t expr, int new) {
	uint32_t node = allocate();
	node_reset_cell(node);
	node_set_tag(node, TAG_LAMBDAABS);
	node_reset_mark(node);
	node_reset_whnf(node);
	node_set_num_checked(node, 0);
	if (new) {
		node_set_new(node); 
	} else {
		node_reset_new(node);
	}

	node_set_first_pointer(node, var);
	node_set_second_pointer(node, expr);

	return node;
}

uint32_t node_create_function(Function fun, int new) {
	uint32_t node = allocate();
	node_reset_cell(node);
	node_set_tag(node, TAG_FUNCTION);
	node_reset_mark(node);
	node_reset_whnf(node);
	if (new) {
		node_set_new(node); 
	} else {
		node_reset_new(node);
	}

	switch (fun) {
		case ADD: case SUB: case MUL: case DIV: case MOD:
		case EQ: case NEQ: case LTH: case GTH: case LEQ: case GEQ:
		case CONS: {
			node_set_num_args(node, 2);
			break;
		}
		case IF: {
			node_set_num_args(node, 3);
			break;
		}
		case HEAD: case TAIL: case Y: {
			node_set_num_args(node, 1);
			break;
		}
		default: {
			break;
		}
	}

	node_set_body(node, fun);

	return node;
}

uint32_t node_create_exp_app(uint32_t first, uint32_t second, int new) {
	uint32_t node = allocate();
	node_reset_cell(node);
	node_set_tag(node, TAG_EXPAPP);
	node_reset_mark(node);
	node_reset_whnf(node);
	node_set_num_checked(node, 0);
	if (new) {
		node_set_new(node); 
	} else {
		node_reset_new(node);
	}

	node_set_first_pointer(node, first);
	node_set_second_pointer(node, second);

	return node;
}

uint8_t get_byte(uint32_t ptr) {
	return (uint8_t) (memory[ptr] & 0xFF);
}

void set_byte(uint32_t ptr, int value) {
	memory[ptr] = (uint8_t) (value & 0xFF);
}

void node_reset_cell(uint32_t ptr_head) {
	set_byte(ptr_head, 0);
	set_byte(ptr_head + 1, 0);
	set_byte(ptr_head + 2, 0);
	set_byte(ptr_head + 3, 0);
	set_byte(ptr_head + 4, 0);
	set_byte(ptr_head + 5, 0);
	set_byte(ptr_head + 6, 0);
	set_byte(ptr_head + 7, 0);
	set_byte(ptr_head + 8, 0);

}

uint32_t node_get_first_pointer(uint32_t ptr_head) {
	return (get_byte(ptr_head + 1) << 24) | 
		   (get_byte(ptr_head + 2) << 16) |
		   (get_byte(ptr_head + 3) << 8)  |
		   (get_byte(ptr_head + 4));
}

void node_set_first_pointer(uint32_t ptr_head, int32_t value) {
	set_byte(ptr_head + 1, value >> 24);
	set_byte(ptr_head + 2, value >> 16);
	set_byte(ptr_head + 3, value >> 8);
	set_byte(ptr_head + 4, value);
}

uint32_t node_get_second_pointer(uint32_t ptr_head) {
	return (get_byte(ptr_head + 5) << 24) |
		   (get_byte(ptr_head + 6) << 16) |
		   (get_byte(ptr_head + 7) << 8)  |
		   (get_byte(ptr_head + 8));
}

void node_set_second_pointer(uint32_t ptr_head, int32_t value) {
	set_byte(ptr_head + 5, value >> 24);
	set_byte(ptr_head + 6, value >> 16);
	set_byte(ptr_head + 7, value >> 8);
	set_byte(ptr_head + 8, value);
}

uint64_t node_get_body(uint32_t ptr_head) {
	return ((uint64_t) node_get_first_pointer(ptr_head) << 32) | node_get_second_pointer(ptr_head);
}

void node_set_body(uint32_t ptr_head, int64_t value) {
	node_set_first_pointer(ptr_head, (int32_t) (value >> 32));
	node_set_second_pointer(ptr_head, (int32_t) (value % ((int64_t) 1 << 32)));
}

Tag node_get_tag(uint32_t ptr_head) {
	return (memory[ptr_head] & MASK_TAG) / (1 << 5);
}

void node_set_tag(uint32_t ptr_head, Tag tag) {
	memory[ptr_head] &= ~MASK_TAG;
	memory[ptr_head] |= (tag << 5);
}

int node_get_mark(uint32_t ptr_head) {
	return (memory[ptr_head] & MASK_MARK) / (1 << 4);
}

void node_set_mark(uint32_t ptr_head) {
	memory[ptr_head] |= MASK_MARK;
}

void node_reset_mark(uint32_t ptr_head) {
	memory[ptr_head] &= ~MASK_MARK;
}

int node_get_whnf(uint32_t ptr_head) {
	return (memory[ptr_head] & MASK_WHNF) / (1 << 3);
}

void node_set_whnf(uint32_t ptr_head) {
	memory[ptr_head] |= MASK_WHNF;
}

void node_reset_whnf(uint32_t ptr_head) {
	memory[ptr_head] &= ~MASK_WHNF;
}

int node_get_num_checked(uint32_t ptr_head) {
	return (memory[ptr_head] & MASK_NUM_CHECKED) / (1 << 1);
}

void node_set_num_checked(uint32_t ptr_head, int num) {
	if (num > 3 || num < 0) {
		printf("ERROR! num_checked out of bounds! num: %d\n", num);
		exit(1);
	}
	memory[ptr_head] &= ~MASK_NUM_CHECKED;
	memory[ptr_head] |= (num << 1);
}

int node_get_is_nil(uint32_t ptr_head) {
	return (memory[ptr_head] & MASK_NIL) / (1 << 2);
}

void node_set_is_nil(uint32_t ptr_head) {
	memory[ptr_head] |= MASK_NIL;
}

void node_reset_is_nil(uint32_t ptr_head) {
	memory[ptr_head] &= ~MASK_NIL;
}

int node_get_num_args(uint32_t ptr_head) {
	return (memory[ptr_head] & MASK_NUM_ARGS) / (1 << 1);
}

void node_set_num_args(uint32_t ptr_head, int num) {
	if (num > 3 || num < 0) {
		printf("ERROR! num_args out of bounds! num: %d\n", num);
		exit(1);
	}
	memory[ptr_head] &= ~MASK_NUM_ARGS;
	memory[ptr_head] |= (num << 1);
}

int node_get_new(uint32_t ptr_head) {
	return memory[ptr_head] & MASK_NEW;
}

void node_set_new(uint32_t ptr_head) {
	memory[ptr_head] |= MASK_NEW;
}

void node_reset_new(uint32_t ptr_head) {
	memory[ptr_head] &= ~MASK_NEW;
}
