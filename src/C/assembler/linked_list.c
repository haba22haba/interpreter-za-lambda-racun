#include "linked_list.h"

input_list_t *input_list = NULL;
subtree_list_t *subtree_list = NULL;

void list_create_input_list() {
	input_list = (input_list_t *) malloc(sizeof(input_list_t));
	input_list->first = NULL;
	input_list->last = NULL;
}

void list_create_subtree_list() {
	subtree_list = (subtree_list_t *) malloc(sizeof(subtree_list_t));
	subtree_list->first = NULL;
	subtree_list->last = NULL;
}

input_element_t *list_input_create_element(char *s, int n) {
	input_element_t *el = (input_element_t *) malloc(sizeof(input_element_t));
	if (el) {
		el->line = s;
		el->line_num = n;
		el->next = NULL;
		return el;
	} else {
		printf("list_input_create_element()\tFailed malloc\n");
		exit(1);
	}
}

subtree_element_t *list_subtree_create_element() {
	subtree_element_t *el = (subtree_element_t *) malloc(sizeof(subtree_element_t));
	if (el) {
		return el;
	} else {
		printf("list_subtree_create_element()\tFailed malloc!\n");
		exit(1);
	}
}

void list_input_add(input_element_t *el) {
	if (input_list->first == NULL) {
		input_list->first = el;
		input_list->last = input_list->first;
	} else {
		input_list->last->next = el;
		input_list->last = el;
		input_list->last->next = NULL;
	}
}

void list_subtree_add(subtree_element_t *el) {
	if (subtree_list->first == NULL) {
		subtree_list->first = el;
		subtree_list->last = subtree_list->first;
	} else {
		subtree_list->last->next = el;
		subtree_list->last = el;
		subtree_list->last->next = NULL;
	}
}

input_element_t *list_input_find_element(int n) {
	input_element_t *el = input_list->first;
	while (el != NULL && el->line_num != n) {
		el = el->next;
	}
	return el;
}

subtree_element_t *list_subtree_find_element(int n) {
	subtree_element_t *el = subtree_list->first;
	while (el != NULL && el->line_num != n) {
		el = el->next;
	}
	return el;
}

void list_input_print() {
	input_element_t *el = input_list->first;
	while (el != NULL) {
		printf("%d %s\n", el->line_num, el->line);
		el = el->next;
	}
}

void list_input_clear() {
	input_element_t *el = input_list->first;
	while (el != NULL) {
		input_element_t *next = el->next;
		free(el->line);
		free(el);
		el = next;
	}
}

void list_subtree_clear() {
	subtree_element_t *el = subtree_list->first;
	while (el != NULL) {
		subtree_element_t *next = el->next;
		free(el);
		el = next;
	}
}