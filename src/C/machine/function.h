#ifndef _FUNCTION_H_
#define _FUNCTION_H_

#include "errors.h"
#include "node.h"
#include "stack.h"

void function_evaluate_state();

void function_handle_three_args();
void function_handle_two_args();
void function_handle_one_arg();

void function_move_right();

void function_handle_if_condition();
void function_handle_if_true();
void function_handle_if_false();

void function_handle_numeric_fst_arg();
void function_handle_numeric_snd_arg();

void function_handle_cons_fst_arg();
void function_handle_cons_snd_arg();

void function_handle_y();

void function_handle_list_arg();

void function_evaluate();

int function_compare_constants(uint32_t a, uint32_t b);

void function_create_y_subtree();

void function_correct_root();

#endif /* _FUNCTION_H_ */