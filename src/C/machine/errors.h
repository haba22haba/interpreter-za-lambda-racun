#ifndef _ERRORS_H_
#define _ERRORS_H_

#include <stdio.h>
#include <stdlib.h>

enum Error {
	ERROR_MEMORY = 1,
	ERROR_REGISTER,
	ERROR_ARGUMENTS,
	ERROR_ATTACH,
	ERROR_STATE,
	ERROR_NULL,
	ERROR_TAG,
	ERROR_MARK,
	ERROR_STACK
};

void error_memory_out_of_bounds();
void error_numargs_out_of_bounds();
void error_illegal_num_arguments();
void error_numchecked_out_of_bounds();
void error_function_result();
void error_unknown_state();
void error_register_null();
void error_unknown_tag();
void error_mark();
void error_out_of_memory();
void error_lambda_result();
void error_stack_overflow();
void error_stack_empty();

#endif /* _ERRORS_H_ */