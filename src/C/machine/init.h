#ifndef _INIT_H_
#define _INIT_H_

#include "registers.h"
#include "errors.h"
#include "stack.h"
#include "machine.h"

void init_evaluate_state();

void init_memory();
void init_registers();

#endif /* _INIT_H_ */