#ifndef _LAMBDA_H_
#define _LAMBDA_H_

#include "node.h"
#include "stack.h"

void lambda_evaluate_state(); 

void lambda_handle_cons_cell();
void lambda_handle_exp_app();
void lambda_handle_lambda_abs();
void lambda_handle_variable();
void lambda_handle_function();
void lambda_handle_constant();

void lambda_correct_root();

#endif /* _LAMBDA_H_ */