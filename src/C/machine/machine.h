#ifndef _MACHINE_H_
#define _MACHINE_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "registers.h"
#include "init.h"
#include "stack.h"
#include "node.h"
#include "unwind.h"
#include "function.h"
#include "lambda.h"
#include "gc.h"

void register_init();
void register_start_init();
void memory_init();

void garbage_collection();
void machine_evaluate();
int is_whnf();

void print_machine_state();
void print_program(uint32_t node);

#endif /* _MACHINE_H_ */
