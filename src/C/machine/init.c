#include "init.h"

void init_evaluate_state() {
	switch (INIT_STATE) {
		case STACK_INIT: {
			stack_init();
			INIT_STATE = MEMORY_INIT;
			EMPTY = (NUM_CELLS - 1) * NODE_SIZE;
			PC = NULL_POINTER;
			break;
		}
		case MEMORY_INIT: {
			init_memory();
			break;
		}
		case REGISTER_INIT: {
			init_registers();
			break;
		}
		default: {
			error_unknown_state();
			break;
		}
	}
}

void init_memory() {
	if (EMPTY == NULL_POINTER) {
		EMPTY = PC;
		INIT_STATE = REGISTER_INIT;
	} else {
		if (node_get_new(EMPTY) == 0) {
			node_set_first_pointer(EMPTY, PC);
			PC = EMPTY;
			EMPTY -= NODE_SIZE;
		} else {
			number_nodes_created++;
			node_reset_new(EMPTY);
			EMPTY -= NODE_SIZE;
		}
	}
}

void init_registers() {
	PC = NODE_SIZE;
	BEGIN = NODE_SIZE;
	START = NODE_SIZE;
	APPCOUNT = 0;
	ROOT = NULL_POINTER;
	ARG = NULL_POINTER;
	VAR = NULL_POINTER;
	FUN = 0;
	NUMARGS = 0;
	FSTOP = NULL_POINTER;
	SNDOP = NULL_POINTER;
	TRDOP = NULL_POINTER;
	RESULT = NULL_POINTER;
	STATE = STATE_UNWIND;
	UNWIND_STATE = UNWIND_NORMAL;
}