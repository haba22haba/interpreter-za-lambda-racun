#ifndef _UNWIND_H_
#define _UNWIND_H_

#include "node.h"
#include "stack.h"
#include "measurements.h"

void unwind_evaluate_state();
void unwind_handle_data_object();
void unwind_handle_exp_app();
void unwind_handle_lambda_abs();
void unwind_handle_function();

#endif /* _UNWIND_H_ */