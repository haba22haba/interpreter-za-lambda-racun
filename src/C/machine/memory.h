#ifndef _MEMORY_H_
#define _MEMORY_H_

#include <stdint.h>

#define NODE_SIZE 9
#define NUM_CELLS (1024 / 4)
#define MEMORY_SIZE (NODE_SIZE * NUM_CELLS) 
#define NULL_POINTER 0

uint8_t memory[MEMORY_SIZE];

#endif /* _MEMORY_H_ */