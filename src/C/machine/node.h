#ifndef _NODE_H_
#define _NODE_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "registers.h"
#include "errors.h"
#include "memory.h"
#include "machine.h"
#include "measurements.h"

/* NODE 
 * 1st byte: HEAD
 * 2-5th byte: FIRST POINTER
 * 6-9th byte: SECOND POINTER
 * */

/* HEAD: 
 * Bits: 1 2 3 |  4   |  5   | 6 7 8 
 *        TAG  | Mark | WHNF | other
 */

#define MASK_TAG            (0xE0)
#define MASK_MARK           (0x10)
#define MASK_WHNF           (0x08)
#define MASK_NUM_CHECKED    (0x06)
#define MASK_NIL            (0x04)
#define MASK_NUM_ARGS       (0x06)
#define MASK_NEW			(0x01)

typedef enum _Function {
	ADD,
	SUB,
	MUL,
	DIV,
	MOD,
	EQ,
	NEQ,
	LTH,
	GTH,
	LEQ,
	GEQ,
	IF,
	HEAD,
	TAIL,
	CONS,
	Y
} Function;


typedef enum _Tag {
	TAG_CONSTANT = 0,
	TAG_CONSCELL = 1,
	TAG_VARIABLE = 2,
	TAG_FUNCTION = 3,
	TAG_LAMBDAABS = 4,
	TAG_EXPAPP = 5,

} Tag;

uint32_t allocate();

uint32_t node_create_constant(uint64_t value, int is_nil, int new);
uint32_t node_create_cons_cell(uint32_t head, uint32_t tail, int new);
uint32_t node_create_variable(int64_t var, int new);
uint32_t node_create_lambda_abs(uint32_t var, uint32_t expr, int new);
uint32_t node_create_function(Function fun, int new);
uint32_t node_create_exp_app(uint32_t first, uint32_t second, int new);

void node_copy_cell(uint32_t dst, uint32_t src);
void node_reset_cell(uint32_t ptr_head);

uint32_t node_get_first_pointer(uint32_t ptr_head);
void node_set_first_pointer(uint32_t ptr_head, int32_t value);

uint32_t node_get_second_pointer(uint32_t ptr_head);
void node_set_second_pointer(uint32_t ptr_head, int32_t value);

uint64_t node_get_body(uint32_t ptr_head);
void node_set_body(uint32_t ptr_head, int64_t value);

Tag node_get_tag(uint32_t ptr_head);
void node_set_tag(uint32_t ptr_head, Tag tag);

int node_get_mark(uint32_t ptr_head);
void node_set_mark(uint32_t ptr_head);
void node_reset_mark(uint32_t ptr_head);

int node_get_whnf(uint32_t ptr_head);
void node_set_whnf(uint32_t ptr_head);
void node_reset_whnf(uint32_t ptr_head);

int node_get_num_checked(uint32_t ptr_head);
void node_set_num_checked(uint32_t ptr_head, int num);

int node_get_is_nil(uint32_t ptr_head);
void node_set_is_nil(uint32_t ptr_head);
void node_reset_is_nil(uint32_t ptr_head);

int node_get_num_args(uint32_t ptr_head);
void node_set_num_args(uint32_t ptr_head, int num);

int node_get_new(uint32_t ptr_head);
void node_set_new(uint32_t ptr_head);
void node_reset_new(uint32_t ptr_head);

#endif /* _NODE_H_ */
