#ifndef _STACK_H_
#define _STACK_H_

#include <stdio.h>
#include <stdlib.h>

#include "registers.h"
#include "errors.h"

#define STACK_SIZE 5000

void stack_init();

void stack_push_numargs();
void stack_pop_numargs();

void stack_push_pc();
void stack_pop_pc();

void stack_push_fstop();
void stack_pop_fstop();

void stack_push_registers();
void stack_pop_registers();

int stack_is_empty();

#endif /* _STACK_H_ */
