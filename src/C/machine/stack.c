#include "stack.h"

int64_t stack[STACK_SIZE];

void stack_init() {
	STACK_POINTER = 0;
	NUM_STATES = 0;
}

void stack_push_pc() {
	if (STACK_POINTER >= STACK_SIZE) {
		error_stack_overflow();
	}
	LASTPC = PC;
	stack[STACK_POINTER++] = (int64_t) PC;
}

void stack_pop_pc() {
	if (STACK_POINTER <= 0) {
		error_stack_empty();
	}
	LASTPC = PC;
	PC = (uint32_t) stack[--STACK_POINTER];
}

void stack_push_numargs() {
	if (STACK_POINTER >= STACK_SIZE) {
		error_stack_overflow();
	}
	stack[STACK_POINTER++] = (int64_t) NUMARGS;
}

void stack_pop_numargs() {
	if (STACK_POINTER <= 0) {
		error_stack_empty();
	}
	NUMARGS = (uint32_t) stack[--STACK_POINTER];
}

void stack_push_fstop() {
	if (STACK_POINTER >= STACK_SIZE) {
		error_stack_overflow();
	}
	stack[STACK_POINTER++] = (int64_t) FSTOP;
}

void stack_pop_fstop() {
	if (STACK_POINTER <= 0) {
		error_stack_empty();
	}
	FSTOP = (uint32_t) stack[--STACK_POINTER];
}


void stack_push_registers() {
	if (STACK_POINTER + 13 >= STACK_SIZE) {
		error_stack_overflow();
	}
	stack[STACK_POINTER++] = (int64_t) PC;
	stack[STACK_POINTER++] = (int64_t) BEGIN;
	stack[STACK_POINTER++] = (int64_t) ROOT;
	stack[STACK_POINTER++] = (int64_t) APPCOUNT;
	stack[STACK_POINTER++] = (int64_t) ARG;
	stack[STACK_POINTER++] = VAR;
	stack[STACK_POINTER++] = FUN;
	stack[STACK_POINTER++] = (int64_t) NUMARGS;
	stack[STACK_POINTER++] = (int64_t) FSTOP;
	stack[STACK_POINTER++] = (int64_t) SNDOP;
	stack[STACK_POINTER++] = (int64_t) TRDOP;
	stack[STACK_POINTER++] = (int64_t) RESULT;
	stack[STACK_POINTER++] = (int64_t) STATE;

	NUM_STATES++;
}

void stack_pop_registers() {
	if (STACK_POINTER - 13 < 0 || NUM_STATES <= 0) {
		error_stack_empty();
	}

	STATE = (State) stack[--STACK_POINTER];
	RESULT = (uint32_t) stack[--STACK_POINTER];
	TRDOP = (uint32_t) stack[--STACK_POINTER];
	SNDOP = (uint32_t) stack[--STACK_POINTER];
	FSTOP = (uint32_t) stack[--STACK_POINTER];
	NUMARGS = (uint32_t) stack[--STACK_POINTER];
	FUN = stack[--STACK_POINTER];
	VAR = stack[--STACK_POINTER];
	ARG = (uint32_t) stack[--STACK_POINTER];
	APPCOUNT = (uint32_t) stack[--STACK_POINTER];
	ROOT = (uint32_t) stack[--STACK_POINTER];
	BEGIN = (uint32_t) stack[--STACK_POINTER];
	PC = (uint32_t) stack[--STACK_POINTER];

	NUM_STATES--;
}

int stack_is_empty() {
	return (NUM_STATES == 0) ? 1 : 0;
}
