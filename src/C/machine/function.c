#include "function.h"

void register_init_PC() {
	BEGIN = PC;
	STATE = STATE_UNWIND;
	APPCOUNT = 0;
	ROOT = NULL_POINTER;
	ARG = NULL_POINTER;
	VAR = NULL_POINTER;
	FUN = 0;
	NUMARGS = 0;
	FSTOP = NULL_POINTER;
	SNDOP = NULL_POINTER;
	TRDOP = NULL_POINTER;
	RESULT = NULL_POINTER;
}

void function_evaluate_state() {
	if (NUMARGS == 3) {
		function_handle_three_args();
	} else if (NUMARGS == 2) {
		function_handle_two_args();
	} else if (NUMARGS == 1) {
		function_handle_one_arg();
	} else if (NUMARGS == 0) {
		function_evaluate();
		function_correct_root();
	} else {
		error_numargs_out_of_bounds();
	}
}

void function_handle_three_args() {
	if (MOVED_RIGHT == 0) {
		function_move_right();
	} else {
		MOVED_RIGHT = 0;
		function_handle_if_condition();
	}
}

void function_move_right() {
	stack_pop_pc();
	node_reset_new(PC);
	node_set_num_checked(PC, 1);
	stack_push_pc();
	PC = node_get_second_pointer(PC);
	MOVED_RIGHT = 1;
}

void function_handle_if_condition() {
	if (node_get_tag(PC) == TAG_CONSTANT) {
		FSTOP = PC;
		stack_pop_pc();
		node_set_num_checked(PC, 0);
		NUMARGS--;
	} else if (node_get_tag(PC) == TAG_EXPAPP) {
		if (node_get_whnf(PC)) {
			node_set_whnf(BEGIN);
			STATE = STATE_UNWIND;
		} else {
			stack_push_registers();
			register_init_PC();
		}
	}
}

void function_handle_two_args() {
	if (MOVED_RIGHT == 0) {
		function_move_right();
	} else {
		MOVED_RIGHT = 0;
		switch ((Function) FUN) {
			case IF: {
				function_handle_if_true();
				break;
			}
			case ADD: case SUB: case DIV: case MUL: case MOD:
			case EQ: case NEQ: case LTH: case GTH: case LEQ: case GEQ: {
				function_handle_numeric_fst_arg();
				break;
			}
			case CONS: {
				function_handle_cons_fst_arg();
				break;
			}
			default: {
				error_illegal_num_arguments();
			}
		}
	}
}

void function_handle_if_true() {
	SNDOP = PC;
	stack_pop_pc();
	node_set_num_checked(PC, 0);
	NUMARGS--;
}

void function_handle_numeric_fst_arg() {
	if (node_get_tag(PC) == TAG_CONSTANT ||
		node_get_tag(PC) == TAG_CONSCELL) {
		FSTOP = PC;
		stack_pop_pc();
		node_set_num_checked(PC, 0);
		NUMARGS--;
	} else if (node_get_tag(PC) == TAG_EXPAPP) {
		if (node_get_whnf(PC)) {
			node_set_whnf(BEGIN);
			STATE = STATE_UNWIND;
		} else {
			stack_push_registers();
			register_init_PC();
		}
	}
}

void function_handle_cons_fst_arg() {
	FSTOP = PC;
	stack_pop_pc();
	node_set_num_checked(PC, 0);
	NUMARGS--;
}

void function_handle_one_arg() {
	if (MOVED_RIGHT == 0) {
		function_move_right();
	} else {
		MOVED_RIGHT = 0;
		switch ((Function) FUN) {
			case IF: {
				function_handle_if_false();
				break;
			}
			case ADD: case SUB: case DIV: case MUL: case MOD:
			case EQ: case NEQ: case LTH: case GTH: case LEQ: case GEQ: {
				function_handle_numeric_snd_arg();
				break;
			}
			case CONS: {
				function_handle_cons_snd_arg();
				break;
			}
			case Y: {
				function_handle_y();
				break;
			}
			case HEAD: case TAIL: {
				function_handle_list_arg();
				break;
			}
			default: {
				error_illegal_num_arguments();
			}
		}
	}
}

void function_handle_if_false() {
	TRDOP = PC;
	stack_pop_pc();
	node_set_num_checked(PC, 0);
	NUMARGS--;
}

void function_handle_numeric_snd_arg() {
	if (node_get_tag(PC) == TAG_CONSTANT ||
		node_get_tag(PC) == TAG_CONSCELL) {
		SNDOP = PC;
		stack_pop_pc();
		node_set_num_checked(PC, 0);
		NUMARGS--;
	} else {
		if (node_get_whnf(PC)) {
			node_set_whnf(BEGIN);
			STATE = STATE_UNWIND;
		} else {
			stack_push_registers();
			register_init_PC();
		}
	}
}

void function_handle_cons_snd_arg() {
	SNDOP = PC;
	stack_pop_pc();
	node_set_num_checked(PC, 0);
	NUMARGS--;
}

void function_handle_y() {
	if (node_get_tag(PC) == TAG_LAMBDAABS) {
		FSTOP = PC;
		stack_pop_pc();
		node_set_num_checked(PC, 0);
		NUMARGS--;
	} else {
		if (node_get_whnf(PC)) {
			node_set_whnf(BEGIN);
			STATE = STATE_UNWIND;
		} else {
			stack_push_registers();
			register_init_PC();
		}
	}
}

void function_handle_list_arg() {
	if (node_get_tag(PC) == TAG_CONSCELL) {
		FSTOP = node_get_first_pointer(PC);
		SNDOP = node_get_second_pointer(PC);
		NUMARGS--;
		stack_pop_pc();
	} else {
		stack_push_registers();
		register_init_PC();
	}
}

void function_evaluate() {
	switch((Function) FUN) {
		case IF: {
			RESULT = (node_get_body(FSTOP) != 0) ? SNDOP : TRDOP;
			break;
		}
		case ADD: {
			RESULT = node_create_constant(node_get_body(FSTOP) + node_get_body(SNDOP), 0, 0);
			break;
		}
		case SUB: {
			RESULT = node_create_constant(node_get_body(FSTOP) - node_get_body(SNDOP), 0, 0);
			break;
		}
		case DIV: {
			RESULT = node_create_constant(node_get_body(FSTOP) / node_get_body(SNDOP), 0, 0);
			break;
		}
		case MUL: {
			RESULT = node_create_constant(node_get_body(FSTOP) * node_get_body(SNDOP), 0, 0);
			break;
		}
		case MOD: {
			RESULT = node_create_constant(node_get_body(FSTOP) % node_get_body(SNDOP), 0, 0);
			break;
		}
		case EQ: {
			RESULT = (function_compare_constants(FSTOP, SNDOP)) ? node_create_constant(1, 0, 0) : node_create_constant(0, 0, 0);
			break;
		}
		case NEQ: {
			RESULT = (!function_compare_constants(FSTOP, SNDOP)) ? node_create_constant(1, 0, 0) : node_create_constant(0, 0, 0);
			break;
		}
		case LTH: {
			RESULT = (node_get_body(FSTOP) < node_get_body(SNDOP)) ? node_create_constant(1, 0, 0) : node_create_constant(0, 0, 0);
			break;
		}
		case GTH: {
			RESULT = (node_get_body(FSTOP) > node_get_body(SNDOP)) ? node_create_constant(1, 0, 0) : node_create_constant(0, 0, 0);
			break;
		}
		case LEQ: {
			RESULT = (node_get_body(FSTOP) <= node_get_body(SNDOP)) ? node_create_constant(1, 0, 0) : node_create_constant(0, 0, 0);
			break;
		}
		case GEQ: {
			RESULT = (node_get_body(FSTOP) >= node_get_body(SNDOP)) ? node_create_constant(1, 0, 0) : node_create_constant(0, 0, 0);
			break;
		}
		case HEAD: {
			RESULT = FSTOP;
			break;
		}
		case TAIL: {
			RESULT = SNDOP;
			break;
		}
		case CONS: {
			RESULT = node_create_cons_cell(FSTOP, SNDOP, 0);
			break;
		}
		case Y: {
			function_create_y_subtree();
			break;
		}
		default:
			break;
	}
}

int function_compare_constants(uint32_t a, uint32_t b) {
	if (node_get_tag(a) == node_get_tag(b) &&
		node_get_tag(a) == TAG_CONSTANT) {
		if (node_get_is_nil(a) == 1 && node_get_is_nil(b) == 1) {
			return 1;
		} else if (node_get_is_nil(a) == 0 && node_get_is_nil(b) == 0) {
			return (node_get_body(a) == node_get_body(b));
		} else {
			return 0;
		}
	} else {
		return 0;
	}
}

void function_create_y_subtree() {
	TRDOP = node_create_function(Y, 1);
	SNDOP = node_create_exp_app(TRDOP, FSTOP, 1);
	RESULT = node_create_exp_app(FSTOP, SNDOP, 1);
}

void function_correct_root() {
	node_copy_cell(PC, RESULT);
	node_reset_cell(RESULT);
	if ((Function) FUN == Y) {
		stack_pop_pc();
		APPCOUNT -= 2;
		STATE = STATE_UNWIND;
	} else {
		if (stack_is_empty()) {
			STATE = STATE_UNWIND;
		} else {
			stack_pop_registers();
		}
	}
}