#include "errors.h"

void error_memory_out_of_bounds() {
	fprintf(stderr, "ERROR\tMemory out of bounds!\n");
	exit(ERROR_MEMORY);
}

void error_numargs_out_of_bounds() {
	fprintf(stderr, "ERROR\tNUMARGS out of bounds!\n");
	exit(ERROR_REGISTER);
}

void error_illegal_num_arguments() {
	fprintf(stderr, "ERROR\tIllegal number of arguments for a function!\n");
	exit(ERROR_ARGUMENTS);
}

void error_numchecked_out_of_bounds() {
	fprintf(stderr, "ERROR\tNUMCHECKED out of bounds!\n");
	exit(ERROR_REGISTER);
}

void error_function_result() {
	fprintf(stderr, "ERROR\tFunction result is not attached to a function application!\n");
	exit(ERROR_ATTACH);
}

void error_unknown_state() {
	fprintf(stderr, "ERROR\tUnknown state!\n");
	exit(ERROR_STATE);
}

void error_register_null() {
	fprintf(stderr, "ERROR\tRegister accessed NULL POINTER!\n");
	exit(ERROR_NULL);
}

void error_unknown_tag() {
	fprintf(stderr, "ERROR\tUnknown tag!\n");
	exit(ERROR_TAG);
}

void error_mark() {
	fprintf(stderr, "ERROR\tMarking in garbage collector phase failed!\n");
	exit(ERROR_MARK);
}

void error_out_of_memory() {
	fprintf(stderr, "ERROR\tOut of memory!\n");
	exit(ERROR_MEMORY);
}

void error_lambda_result() {
	fprintf(stderr, "ERROR\tLambda abstraction not attached to a function application!\n");
	exit(ERROR_ATTACH);
}

void error_stack_overflow() {
	fprintf(stderr, "ERROR!\tStack overflow!\n");
	exit(ERROR_STACK);
}

void error_stack_empty() {
	fprintf(stderr, "ERROR!\tEmpty stack!\n");
	exit(ERROR_STACK);
}