#include <stdio.h>
#include <stdlib.h>

#include "machine.h"
#include "stack.h"
#include "time.h"
#include "measurements.h"

void read_memory();
void write_report(double t);

int main() {
	read_memory();

	clock_t begin = clock();
	number_functions_reduced = 0;
	number_garbage_collector = 0;
	number_lambda_abstractions = 0;
	number_nodes_created = 0;

	STATE = STATE_INIT;
	INIT_STATE = STACK_INIT;

	machine_evaluate();

	clock_t end = clock();

	print_program(START);
	printf("\n");

	double time_spent = (double) (end - begin) / CLOCKS_PER_SEC;
	write_report(time_spent);
	return 0;
}

void read_memory() {
	FILE *mem = fopen("memory.bin", "r");
	fread(memory, 1, sizeof(memory), mem);
	fclose(mem);
}

void write_report(double t) {
	FILE *f = fopen("report.txt", "a");
	fprintf(f, "MACHINE EXECUTION\n");
	fprintf(f, "Time elapsed: %f seconds\n", t);
	fprintf(f, "Number of nodes used: %d\n", number_nodes_created);
	fprintf(f, "Number of lambda abstractions reduced: %d\n", number_lambda_abstractions);
	fprintf(f, "Number of built-in functions reduced: %d\n", number_functions_reduced);
	fprintf(f, "Number of times garbage collector was invoked: %d\n", number_garbage_collector);
	fprintf(f, "\n");
	fclose(f);
}
