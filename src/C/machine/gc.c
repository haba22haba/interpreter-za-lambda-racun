#include "gc.h"

void gc_prepare() {
	stack_push_registers();
	PC = START;
	STATE = STATE_GARBAGE_COLLECTION;
	GC_STATE = GC_MARK;
	GC_MARK_DONE = 0;
	number_garbage_collector++;
}

void gc_evaluate_state() {
	switch ((Garbage_state) GC_STATE) {
		case GC_MARK: {
			gc_mark();
			break;
		}
		case GC_SWEEP_FST: {
			gc_sweep_fst_phase();
			break;
		}
		case GC_SWEEP_SND: {
			gc_sweep_snd_phase();
			break;
		}
		case GC_RESTORE: {
			gc_restore();
			break;
		}
		default: {
			error_unknown_state();
			break;
		}
	}
}

void gc_mark() {
	if (GC_MARK_DONE == 1) {
		GC_STATE = GC_SWEEP_FST;
		EMPTY = (NUM_CELLS - 1) * NODE_SIZE;
	} else {
		if (START == PC) {
			gc_mark_fst_node();
		} else {
			switch (node_get_tag(PC)) {
				case TAG_CONSCELL:
				case TAG_LAMBDAABS:
				case TAG_EXPAPP: {
					gc_handle_inner_node();
					break;
				}
				case TAG_VARIABLE:
				case TAG_FUNCTION:
				case TAG_CONSTANT: {
					gc_handle_leaf();
					break;
				}
			}
		}
	}
}

void gc_mark_fst_node() {
	switch (node_get_tag(START)) {
		case TAG_EXPAPP:
		case TAG_LAMBDAABS:
		case TAG_CONSCELL: {
			if (node_get_mark(START) == 0 && node_get_new(START) == 0) {
				NUMARGS = node_get_num_checked(START);
				stack_push_numargs();
				node_set_new(START);
				node_set_mark(START);
				node_set_num_checked(START, 1);
				stack_push_pc();
				PC = node_get_first_pointer(START);
				GC_MARK_DONE = 0;
			} else if ((node_get_new(START) == 1) && (node_get_mark(START) == 1)) { 
				if (node_get_num_checked(START) == 1) {
					if (LASTPC == PC) {
						stack_pop_pc();
						LASTPC = NULL_POINTER;
					} else {
						node_set_num_checked(START, 2);
						stack_push_pc();
						PC = node_get_second_pointer(START);
						GC_MARK_DONE = 0;
					}
				} else if (node_get_num_checked(START) == 2) {
					if (LASTPC == PC) {
						stack_pop_pc();
						LASTPC = NULL_POINTER;
					} else {
						stack_pop_numargs();
						node_set_num_checked(START, NUMARGS);
						node_reset_new(START);
						GC_MARK_DONE = 1;
					}
				} else {
					error_mark();
				}
			} else if (node_get_new(PC) == 1 && node_get_mark(PC) == 0) {
				node_reset_new(PC);
				GC_MARK_DONE = 0;
			}  else {
				GC_MARK_DONE = 1;
			}
			break;
		}
		case TAG_VARIABLE:
		case TAG_FUNCTION:
		case TAG_CONSTANT: {
			if (node_get_mark(START) == 0 && node_get_new(START) == 0) {
				node_set_mark(START);
			} else if (node_get_mark(START) == 1 && node_get_new(START) == 1) {
				error_mark();
			}
			GC_MARK_DONE = 1;
			break;
		}
		default: {
			error_unknown_tag();
			break;
		}
	}
}

void gc_handle_inner_node() {
	if (node_get_new(PC) == 0 && node_get_mark(PC) == 0) {
		NUMARGS = node_get_num_checked(PC);
		stack_push_numargs();
		node_set_new(PC);
		node_set_mark(PC);
		node_set_num_checked(PC, 1);
		stack_push_pc();
		PC = node_get_first_pointer(PC);
	} else if (node_get_new(PC) == 1 && node_get_mark(PC) == 1) {
		if (node_get_num_checked(PC) == 1) {
			if (LASTPC == PC) {
				stack_pop_pc();
				LASTPC = NULL_POINTER;
			} else {
				node_set_num_checked(PC, 2);
				stack_push_pc();
				PC = node_get_second_pointer(PC);
			}
		} else if (node_get_num_checked(PC) == 2) {
			if (LASTPC == PC) {
				stack_pop_pc();
				LASTPC = NULL_POINTER;
			} else {
				stack_pop_numargs();
				node_set_num_checked(PC, NUMARGS);
				node_reset_new(PC);
				stack_pop_pc();
			}
		} else {
			error_mark();
		}
	} else if (node_get_new(PC) == 1 && node_get_mark(PC) == 0) {
		node_reset_new(PC);
	} else {
		stack_pop_pc();
	}
}

void gc_handle_leaf() {
	if (node_get_mark(PC) == 0 &&
		node_get_new(PC) == 0) {
		node_set_mark(PC);
		stack_pop_pc();
	} else if (node_get_mark(PC) == 0 && node_get_new(PC) == 1) {
		node_reset_new(PC);
	} else if (node_get_mark(PC) == 1 && node_get_new(PC) == 0) {
		stack_pop_pc();
	} else {
		error_mark();
	}
}

void gc_sweep_fst_phase() {
	if (EMPTY == NULL_POINTER) {
		error_out_of_memory();
	} else {
		if (node_get_mark(EMPTY) == 0) {
			if (node_get_new(EMPTY) == 0) {
				node_set_first_pointer(EMPTY, NULL_POINTER);
				PC = EMPTY;
				EMPTY -= NODE_SIZE;
				GC_STATE = GC_SWEEP_SND;
			} else {
				EMPTY -= NODE_SIZE;
			}
		} else {
			node_reset_mark(EMPTY);
			EMPTY -= NODE_SIZE;
		}
	}
}

void gc_sweep_snd_phase() {
	if (EMPTY == NULL_POINTER) {
		GC_STATE = GC_RESTORE;
		EMPTY = PC;
	} else {
		if (node_get_new(EMPTY) == 0 && node_get_mark(EMPTY) == 0) {
			node_reset_cell(EMPTY);
			node_set_first_pointer(EMPTY, PC);
			PC = EMPTY;
		} else if (node_get_new(EMPTY) == 0 && node_get_mark(EMPTY) == 1) {
			node_reset_mark(EMPTY);
		} else if (node_get_new(EMPTY) == 1 && node_get_mark(EMPTY) == 0) {
			node_reset_mark(EMPTY);
		} else {
			error_mark();
		}
		EMPTY -= NODE_SIZE;
	}
}

void gc_restore() {
	stack_pop_registers();
}