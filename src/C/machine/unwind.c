#include "unwind.h"


void unwind_evaluate_state() {
	if (UNWIND_STATE == UNWIND_NORMAL) {
		switch (node_get_tag(PC)) {
			case TAG_CONSTANT:
			case TAG_CONSCELL: {
				unwind_handle_data_object();
				break;
			}
			case TAG_EXPAPP: {
				unwind_handle_exp_app();
				break;
			}
			case TAG_LAMBDAABS: {
				unwind_handle_lambda_abs();
				break;
			}
			case TAG_VARIABLE: {
				printf("ERROR! Unresolved variable!\n");
				exit(1);
				break;
			}
			case TAG_FUNCTION: {
				unwind_handle_function();
				break;
			}
			default: {
				printf("ERROR! UNKNOWN TAG!\n");
				exit(1);
			}
		}
	} else if (UNWIND_STATE == UNWIND_CHECKING_FUN) {
		if (APPCOUNT == 0) {
			if (!stack_is_empty()) {
				stack_pop_registers();
			}
			UNWIND_STATE = UNWIND_NORMAL;
		} else {
			stack_pop_pc();
			node_set_whnf(PC);
			APPCOUNT--;
		}
	}
}


void unwind_handle_data_object() {
	node_set_whnf(PC);
	if (APPCOUNT == 0) {
		if (!stack_is_empty()) {
			stack_pop_registers();
		}
	} else {
		printf("ERROR! Something is applied to a data object!\n");
		exit(1);
	}
}

void unwind_handle_exp_app() {
	stack_push_pc();
	APPCOUNT++;
	PC = node_get_first_pointer(PC);
}

void unwind_handle_lambda_abs() {
	if (APPCOUNT < 1) {
		node_set_whnf(PC);
		if (!stack_is_empty()) {
			stack_pop_registers();
		}
	} else {
		stack_pop_pc();
		ARG = node_get_second_pointer(PC);
		stack_push_pc();
		PC = node_get_first_pointer(PC);
		VAR = node_get_first_pointer(PC);
		ROOT = PC;
		stack_push_pc();
		PC = node_get_second_pointer(PC);
		NUMARGS = 0;
		stack_push_numargs();
		
		FSTOP = NULL_POINTER;
		SNDOP = NULL_POINTER;
		TRDOP = NULL_POINTER;
		RESULT = NULL_POINTER;
		STATE = STATE_LAMBDA_RESOLVE;
		POP_NUMARGS = 1;

		number_lambda_abstractions++;
	}
}

void unwind_handle_function() {
	if (APPCOUNT < node_get_num_args(PC)) {
		node_set_whnf(PC);
		UNWIND_STATE = UNWIND_CHECKING_FUN;
	} else {
		FUN = node_get_body(PC);
		NUMARGS = node_get_num_args(PC);

		FSTOP = NULL_POINTER;
		SNDOP = NULL_POINTER;
		TRDOP = NULL_POINTER;
		RESULT = NULL_POINTER;
		STATE = STATE_FUNCTION_RESOLVE;
		MOVED_RIGHT = 0;

		number_functions_reduced++;
	}
}
