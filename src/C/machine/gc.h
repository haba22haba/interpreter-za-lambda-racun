#ifndef _GARBAGE_H_
#define _GARBAGE_H_

#include "errors.h"
#include "node.h"
#include "stack.h"

void gc_prepare();

void gc_evaluate_state();

void gc_mark();
void gc_mark_fst_node();
void gc_handle_inner_node();
void gc_handle_leaf();

void gc_sweep_fst_phase();
void gc_sweep_snd_phase();

void gc_restore();

#endif /* _GARBAGE_H_ */