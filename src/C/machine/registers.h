#ifndef _REGISTERS_H_
#define _REGISTERS_H_

#include <stdint.h>

typedef enum _State {
	STATE_INIT,
	STATE_UNWIND,
	STATE_LAMBDA_RESOLVE,
	STATE_FUNCTION_RESOLVE,
	STATE_GARBAGE_COLLECTION,
} State;

typedef enum Init_state {
	STACK_INIT,
	MEMORY_INIT,
	REGISTER_INIT,
} Init_state;

typedef enum _Unwind_state {
	UNWIND_NORMAL,
	UNWIND_CHECKING_FUN,
} Unwind_state;

typedef enum _Garbage_state {
	GC_MARK,
	GC_SWEEP_FST,
	GC_SWEEP_SND,
	GC_RESTORE,
} Garbage_state;

uint32_t START;
uint32_t PC;
uint32_t BEGIN;
uint32_t ROOT;
uint32_t APPCOUNT;
uint32_t ARG;
uint32_t VAR;
int64_t FUN;
uint32_t NUMARGS;
uint32_t FSTOP;
uint32_t SNDOP;
uint32_t TRDOP;
uint32_t RESULT;
State STATE;
uint32_t EMPTY;
uint32_t SAVERES; // for saving the root (we can't save it to the stack)
uint32_t LASTPC;

/* Registers for initialization */
Init_state INIT_STATE;

/* Registers for unwind */
Unwind_state UNWIND_STATE;

/* Registers for function resolve */
uint32_t MOVED_RIGHT;

uint32_t POP_NUMARGS;

/* Registers for garbage collection */
Garbage_state GC_STATE;
uint32_t GC_MARK_DONE;

/* Registers for stack */
uint32_t STACK_POINTER;
uint32_t NUM_STATES;

#endif /* _REGISTERS_H_ */