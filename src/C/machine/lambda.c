#include "lambda.h"

void lambda_evaluate_state() {
	switch (node_get_tag(PC)) {
		case TAG_CONSCELL: {
			lambda_handle_cons_cell();
			break;
		}
		case TAG_EXPAPP: {
			lambda_handle_exp_app();
			break;
		}
		case TAG_LAMBDAABS: {
			lambda_handle_lambda_abs();
			break;
		}
		case TAG_VARIABLE: {
			lambda_handle_variable();
			break;
		}
		case TAG_FUNCTION: {
			lambda_handle_function();
			break;
		}
		case TAG_CONSTANT: {
			lambda_handle_constant();
			break;
		}
	}
}

void lambda_handle_cons_cell() {
	if (node_get_num_checked(PC) == 0) {
		node_set_num_checked(PC, 1);
		NUMARGS = 0;
		stack_push_pc();
		stack_push_numargs();
		PC = node_get_first_pointer(PC);
	} else if (node_get_num_checked(PC) == 1) {
		if (LASTPC == PC) {
			stack_pop_numargs();
			stack_pop_pc();
			LASTPC = NULL_POINTER;
		} else {
			node_set_num_checked(PC, 2);
			NUMARGS = 1;
			stack_push_fstop();
			stack_push_pc();
			stack_push_numargs();
			PC = node_get_second_pointer(PC);
		}
	} else if (node_get_num_checked(PC) == 2) {
		if (LASTPC == PC) {
			stack_pop_numargs();
			stack_pop_pc();
			LASTPC = NULL_POINTER;
		} else {
			if (POP_NUMARGS == 1) {
				stack_pop_fstop();
				RESULT = node_create_cons_cell(FSTOP, SNDOP, 1);
				stack_pop_numargs();
				POP_NUMARGS = 0;
			} else {
				node_set_num_checked(PC, 0);
				POP_NUMARGS = 1;
				if (NUMARGS == 0) {
					FSTOP = RESULT;
					stack_pop_pc();
				} else if (NUMARGS == 1) {
					SNDOP = RESULT;
					stack_pop_pc();
				} else {
					error_numargs_out_of_bounds();
				}
			}
		}
	} else {
		error_numchecked_out_of_bounds();
	}
}

void lambda_handle_exp_app() {
	if (node_get_num_checked(PC) == 0) {
		node_set_num_checked(PC, 1);
		NUMARGS = 0;
		stack_push_pc();
		stack_push_numargs();
		PC = node_get_first_pointer(PC);
	} else if (node_get_num_checked(PC) == 1) {
		if (LASTPC == PC) {
			stack_pop_numargs();
			stack_pop_pc();
			LASTPC = NULL_POINTER;
		} else {
			node_set_num_checked(PC, 2);
			NUMARGS = 1;
			stack_push_fstop();
			stack_push_pc();
			stack_push_numargs();
			PC = node_get_second_pointer(PC);
		}
	} else if (node_get_num_checked(PC) == 2) {
		if (LASTPC == PC) {
			stack_pop_numargs();
			stack_pop_pc();
			LASTPC = NULL_POINTER;
		} else {
			if (POP_NUMARGS == 1) {
				stack_pop_fstop();
				RESULT = node_create_exp_app(FSTOP, SNDOP, 1);
				stack_pop_numargs();
				POP_NUMARGS = 0;
			} else {
				POP_NUMARGS = 1;
				node_set_num_checked(PC, 0);
				if (NUMARGS == 0) {
					FSTOP = RESULT;
					stack_pop_pc();
				} else if (NUMARGS == 1) {
					SNDOP = RESULT;
					stack_pop_pc();
				} else {
					error_numchecked_out_of_bounds();
				}
			}
		}
	} else {
		error_numchecked_out_of_bounds();
	}
}



void lambda_handle_lambda_abs() {
	if (PC == ROOT) {
		node_set_num_checked(PC, 0);
		lambda_correct_root();
	} else {
		if (node_get_first_pointer(PC) == VAR) {
			if (POP_NUMARGS == 1) {
				stack_pop_numargs();
				POP_NUMARGS = 0;
			} else {
				node_set_num_checked(PC, 0);
				POP_NUMARGS = 1;
				if (NUMARGS == 0) {
					FSTOP = PC;
					stack_pop_pc();
				} else if (NUMARGS == 1) {
					SNDOP = PC;
					stack_pop_pc();
				} else {
					error_numargs_out_of_bounds();
				}
			}
		} else {
			if (node_get_num_checked(PC) == 0) {
				node_set_num_checked(PC, 1);
				stack_push_pc();
				PC = node_get_second_pointer(PC);
				NUMARGS = 0;
				stack_push_numargs();
			} else {
				if (POP_NUMARGS == 1) {
					stack_pop_numargs();
					POP_NUMARGS = 0;
				} else {
					node_set_num_checked(PC, 0);
					POP_NUMARGS = 1;
					if (NUMARGS == 0) {
						FSTOP = node_create_lambda_abs(node_get_first_pointer(PC), FSTOP, 1);
						stack_pop_pc();
					} else if (NUMARGS == 1) {
						SNDOP = node_create_lambda_abs(node_get_first_pointer(PC), FSTOP, 1);
						stack_pop_pc();
					} else {
						error_numargs_out_of_bounds();
					}
				}
			}
		}
	}
}	

void lambda_correct_root() {
	if (APPCOUNT > 1) {
		stack_pop_pc();
		APPCOUNT -= 2;
		node_copy_cell(PC, FSTOP);
		stack_pop_pc();
		STATE = STATE_UNWIND;
	} else {
		stack_pop_pc();
		APPCOUNT--;
		node_copy_cell(PC, FSTOP);
		STATE = STATE_UNWIND;
	}
}

void lambda_handle_variable() {
	if (POP_NUMARGS == 1) {
		stack_pop_numargs();
		POP_NUMARGS = 0;
	} else {
		POP_NUMARGS = 1;
		if (NUMARGS == 0) {
			if (node_get_body(PC) == node_get_body(VAR)) {
				FSTOP = ARG;
			} else {
				FSTOP = node_create_variable(node_get_body(PC), 1);
			}
		} else if (NUMARGS == 1) {
			if (node_get_body(PC) == node_get_body(VAR)) {
				SNDOP = ARG;
			} else {
				SNDOP = node_create_variable(node_get_body(PC), 1);
			}
		} else {
			error_numargs_out_of_bounds();
		}
		stack_pop_pc();
	}
}

void lambda_handle_constant() {
	if (POP_NUMARGS == 1) {
		stack_pop_numargs();
		POP_NUMARGS = 0;
	} else {
		POP_NUMARGS = 1;
		if (NUMARGS == 0) {
			FSTOP = node_create_constant(node_get_body(PC), node_get_is_nil(PC), 1);
		} else if (NUMARGS == 1) {
			SNDOP = node_create_constant(node_get_body(PC), node_get_is_nil(PC), 1);
		} else {
			error_numargs_out_of_bounds();
		}
		stack_pop_pc();
	}
}

void lambda_handle_function() {
	if (POP_NUMARGS == 1) {
		stack_pop_numargs();
		POP_NUMARGS = 0;
	} else {
		POP_NUMARGS = 1;
		if (NUMARGS == 0) {
			FSTOP = node_create_function(node_get_body(PC), 1);
		} else if (NUMARGS == 1) {
			SNDOP = node_create_function(node_get_body(PC), 1);
		} else {
			error_numargs_out_of_bounds();
		}
		stack_pop_pc();
	}
}