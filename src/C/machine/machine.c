#include "machine.h"

void register_start_init() {
	START = BEGIN;
}

void machine_evaluate() {
	while (!is_whnf()) {
		if (EMPTY == NULL_POINTER && STATE != STATE_GARBAGE_COLLECTION && STATE != STATE_INIT) {
			gc_prepare();
		} else {
			switch (STATE) {
				case STATE_INIT: {
					init_evaluate_state();
					break;
				}
				case STATE_UNWIND: {
					node_reset_new(PC);
					unwind_evaluate_state();
					break;
				}

				case STATE_LAMBDA_RESOLVE: {
					node_reset_new(PC);
					lambda_evaluate_state();
					break;
				}

				case STATE_FUNCTION_RESOLVE: {
					node_reset_new(PC);
					function_evaluate_state();
					break;
				}

				case STATE_GARBAGE_COLLECTION: {
					gc_evaluate_state();
					break;
				}

				default: {
					error_unknown_state();
					break;
				}
			}
		}
	}

}

int is_whnf() {
	return (STATE == STATE_UNWIND && UNWIND_STATE == UNWIND_NORMAL) ? node_get_whnf(BEGIN) : 0;
}

void print_machine_state() {
	printf("---------------------------------------------\n");
	printf("MACHINE:\n");
	printf("START: %d start: ", START);
	print_program(START);
	printf("\n");
	printf("STATE: %d\n", STATE);
	printf("PC index:%u, program: ", PC);
	print_program(PC);
	printf("\n");
	printf("BEGIN index: %u program: \n", BEGIN);
	print_program(BEGIN);
	printf("\n");
	printf("ROOT index: %u NODE: ", ROOT);
	print_program(ROOT);
	printf("\n");
	printf("APPCOUNT: %u\n", APPCOUNT);
	printf("ARG index: %u, NODE: ", ARG);
	print_program(ARG);
	printf("\n");
	printf("VAR: %u\n", VAR);
	printf("FUN: %ld\n", FUN);
	printf("NUMARGS: %u\n", NUMARGS);
	printf("FSTOP index: %u, NODE: ", FSTOP);
	print_program(FSTOP);
	printf("\n");
	printf("SNDOP index: %u, NODE: ", SNDOP);
	print_program(SNDOP);
	printf("\n");
	printf("TRDOP index: %u\n", TRDOP);
	printf("RESULT index: %u NODE: ", RESULT);
	print_program(TRDOP);
	printf("\n");
	printf("EMPTY index: %u\n", EMPTY);
	printf("SAVERES index: %u\n", SAVERES);
	printf("\n");
}

void print_program(uint32_t node) {
	Tag tag = node_get_tag(node);
	switch (tag) {
		case TAG_CONSTANT:
			if (node_get_is_nil(node)) {
				printf("NIL");
			} else {
				printf("%ld", (int64_t) node_get_body(node));
			}
			break;
		case TAG_FUNCTION:
			switch ((Function) node_get_body(node)) {
				case ADD:
					printf("+");
					break;
				case SUB:
					printf("-");
					break;
				case MUL:
					printf("*");
					break;
				case DIV:
					printf("/");
					break;
				case MOD:
					printf("%%");
					break;
				case EQ:
					printf("==");
					break;
				case NEQ:
					printf("!=");
					break;
				case LTH:
					printf("<");
					break;
				case GTH:
					printf(">");
					break;
				case LEQ:
					printf("<=");
					break;
				case GEQ:
					printf(">=");
					break;
				case HEAD:
					printf("HEAD");
					break;
				case TAIL:
					printf("TAIL");
					break;
				case CONS:
					printf("CONS");
					break;
				case Y:
					printf("Y");
					break;
				case IF:
					printf("IF");
					break;
			}
			break;
		case TAG_CONSCELL:
			printf("( ");
			print_program(node_get_first_pointer(node));
			printf(" ) : ( ");
			print_program(node_get_second_pointer(node));
			printf(" )");
			break;
		case TAG_LAMBDAABS:
			printf("\\ ");
			print_program(node_get_first_pointer(node));
			printf(" . ");
			print_program(node_get_second_pointer(node));
			break;
		case TAG_VARIABLE:
			printf("V:%ld", node_get_body(node));
			break;
		case TAG_EXPAPP:
			printf("@ ( ");
			print_program(node_get_first_pointer(node));
			printf(" ) ( ");
			print_program(node_get_second_pointer(node));
			printf(" )");
			break;
		default:
			break;
	}
}